# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# + deletable=false
# %autoreload 2

# + deletable=false slideshow={"slide_type": "slide"}
import numpy as np
import os
os.environ['GMT_LIBRARY_PATH']='/home/andreid/mambaforge/envs/pygmt/lib'
import pygmt
import xarray as xr
import h5py
import json

from salvus.flow import api, simple_config
from salvus.mesh import simple_mesh, models_1D
import salvus.namespace as sn

from toolbox.grid_tools    import get_regular_station_grid
from toolbox.containers    import Paths, Parameters
from toolbox.constants     import EARTH_R, CMB_R, INNER_CORE_R, EARTH_MASS, G
from toolbox.meshing_tools import tell_me_about_mesh

complex_components = ['re', 'im']
displ_components   = ['V', 'N', 'E']

# + deletable=false
lat_step = 1
lon_step = 1

station_grid = get_regular_station_grid(lat_step = lat_step, lon_step=lon_step, fields = ['displacement', 'gradient-of-displacement'], 
                                        latmin=-89,latmax=90, lonmin=-180, lonmax=179)


# + deletable=false
def create_mesh(paths: dict, parameters: dict):
    ms = simple_mesh.TidalLoading()
    #loading file path and which lmax is to be read
    ms.basic.tidal_loading_file      = str(paths['loading_file'].absolute())
    ms.basic.tidal_loading_lmax_1    = parameters['LMAX']
    ms.basic.local_refinement_level  = parameters['REFINEMENT_LEVEL']
    ms.basic.mantle_refinement_index = 2
    #this number defines resolution of the mesh - divided by 3 it tells you how many elements are along an edge of the cube sphere
    ms.basic.nex                     = parameters['NEX']
    #1D model that's used to create the mesh
    ms.basic.model                   = parameters['ONED_MODEL']
    ms.basic.nelem_vertical          = parameters['NELEM_VERTICAL']
    ms.advanced.tensor_order         = parameters['TENSOR_ORDER']
    
    if parameters['TOPOGRAPHY']:
        ms.topography.topography_file    = paths['surface_topo']
        ms.topography.topography_varname = 'topography_earth2014_egm2008_lmax_256_lmax_256'
    if parameters['MOHO_TOPO']:
        ms.topography.moho_topography_file    = paths['moho_topo']
        ms.topography.moho_topography_varname = 'moho_topography_crust_1_0_egm2008_lmax_256'
    
    if parameters['GRAVITY']:
        ms.gravity_mesh.add_exterior_domain = True
        ms.gravity_mesh.nelem_buffer_outer  = parameters['NELEM_BUFFER_OUTER']
    
    mesh = ms.create_mesh(verbose=True)

    zeros = np.zeros((mesh.nelem, mesh.nodes_per_element))
    mesh.attach_field('zeros', zeros)
    fluid = np.zeros(mesh.nelem)
    mesh.attach_field('fluid', fluid)
    
    del mesh.elemental_fields['QKAPPA']
    del mesh.elemental_fields['QMU']
    
    return mesh
    
def find_sideset(mesh, radius, tolerance=30., sideset_name='r0'):
    '''
    That's a function which is attempting to find the CMB surface and add it to the mesh as a sideset. If you need to find a different surface, you need to play around with the dist() function. 
    '''
    if sideset_name in mesh.side_sets.keys(): print(f'"{sideset_name}" is already attached'); return 0
    def dist(x):
        return np.abs(radius - np.linalg.norm(x, axis=-1)) 
    mesh.find_side_sets_generic(sideset_name, dist, tolerance=tolerance)
    print(f'"{sideset_name}" has been found') if sideset_name in mesh.side_sets.keys()\
    else print('Resolution is not fine enough to find the surface. Increase the tolerance')
    
def prepare_gravity_mesh(input_mesh):
    '''
    add necessary fields and sidesets for the gravity simulation and return a copy of the mesh. 
    The input mesh stays untouched
    '''
    mesh = input_mesh.copy()
    f  = np.ones_like(mesh.elemental_fields['RHO'])
    mesh.elemental_fields['fluid'] = np.ones(mesh.nelem)
    M1 = 1 / (4 * np.pi * G)
    mesh.attach_field('M0' , f)
    mesh.attach_field('M1' , M1*f)
    
    for key in ['VP', 'VS',
                'GRAD_PHI_X', 'GRAD_PHI_Y', 'GRAD_PHI_Z', 'g',
                'zeros','external', 'tidal_elevation_amp', 
                'tidal_elevation_re', 'tidal_elevation_im', 
                'tidal_elevation_sea_mask_grad']: 
        if key in mesh.elemental_fields.keys(): del mesh.elemental_fields[key]
    return mesh

def get_theoretical_potential(radius, model_1d):
    try:
        model_1d                 = models_1D.model.built_in(model_1d)
    except FileNotFoundError:
        model_1d                 = models_1D.model.read(model_1d)
    outer_boundary_rel_radius = radius / EARTH_R
    outer_boundary_potential  = model_1d.get_gravitational_potential(radius=np.array([outer_boundary_rel_radius]))[0]
    point_mass_potential      = G * EARTH_MASS / (outer_boundary_rel_radius * EARTH_R) * (-1)
    print(f'External Dirichlet boundary:\n\tRadius: {outer_boundary_rel_radius} x R_Earth \n\t1D gravity potential: {outer_boundary_potential} J/kg\n\tPoint-mass potential: {point_mass_potential} J/kg')
    return outer_boundary_potential 

def gravity_config( mfile:str, vout:str, polynom_order:int, oned_model:str, stations:list=[], pout:str=''):
    '''
        Configure a poisson simulation. 
            mfile    - mesh file path
            vout     - volumetric output file path
            pout     - pointwise output path
            stations - list of SideSetPoint3D objects
            cowling_or_full - cowling or full gravity
    requires M0, M1, RHS, elemental fields attached to the mesh. 
    Depending on the type of the used boundary condition, might also require NEUMANN field and additional sidesets.
    "fluid" elemental field must be set to ones
    '''
    mesh                                                  = sn.UnstructuredMesh.from_h5(mfile)
    sim                                                   = sn.simple_config.simulation.Poisson(mesh=mesh)

    sim.domain.polynomial_order                           = polynom_order
    sim.physics.poisson_equation.right_hand_side.filename = str(mfile)
    sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
    sim.physics.poisson_equation.right_hand_side.field    = "RHO"

    outer_boundary_potential = get_theoretical_potential(np.max( np.linalg.norm(mesh.points, axis=-1) ) , model_1d = oned_model)
    boundaries               = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"], value = float(outer_boundary_potential))
    sim.add_boundary_conditions(boundaries)
    
    sim.physics.poisson_equation.solution.filename   = os.path.basename(vout)
    sim.physics.poisson_equation.solution.fields     = ["solution", "gradient-of-phi", 'right-hand-side']
    
    return sim
        
def elastic_config( mfile:str, vout:str, polynom_order:int, cowling_or_full='cowling'):
    """
        Configure an elastic simulation. 
            mfile    - mesh file path
            vout     - volumetric output file path
            pout     - pointwise output path
            stations - list of SideSetPoint3D objects
            cowling_or_full - cowling or full gravity
    """
    
    mesh                                             = mfile
    sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
    sim.domain.polynomial_order                      = polynom_order

    # Cowling approximation ('cowling') or full gravity ('full')
    sim.physics.elastostatic_equation.gravity                  = cowling_or_full
    sim.physics.elastostatic_equation.right_hand_side.filename = mfile
    sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
    #Here we put the body force field. In surface loading problems it's zero everythere. 'zeros' is an elemental field of the mesh that we added before.
    if cowling_or_full == 'cowling':
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros"]*3
    elif cowling_or_full == 'full':
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros"]*4
    else:
        raise ValueError('allowed simulation types are "cowling" and "full"')
        
    if len(vout)>0: 
        sim.physics.elastostatic_equation.solution.filename        = os.path.basename(vout)
        sim.physics.elastostatic_equation.solution.fields          = ["solution", 'gradient-of-displacement']

    if    cowling_or_full=='full'   : dirichlet_sidesets = ['r0', 'r2']
    elif  cowling_or_full=='cowling': dirichlet_sidesets = ['r0']
    dirichlet_bc = simple_config.boundary.HomogeneousDirichlet( side_sets=dirichlet_sidesets )
    sim.add_boundary_conditions(dirichlet_bc)
    
    neumann_bc   = simple_config.boundary.Neumann( side_sets=["r1"] )
    sim.add_boundary_conditions(neumann_bc)

    return sim

def add_stations(sim, stations, output_file):
    """Adds stations to a simulation object. 
    output_file should include only the name of the file, not the path to it
    """
    
    if hasattr( stations, '__iter__') * (len(stations) > 0):
        sim.add_receivers(stations)
        sim.output.point_data.format                          = "hdf5"
        sim.output.point_data.filename                        = output_file
        sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations 
        print(f'{len(stations)} stations were added to the simulation')

def run_simulation(sim, directory:str, parameters:dict, stations=[], pout=''):
    """
    Run a simulation. 
            vout       - volumetric output file path
            parameters - simulation parameters
       parameters must be a dictionary including the following keys:
    {'MAX_ITERATIONS': ,
     'REL_TOLERANCE' : ,
     'SITENAME'      : ,
     'NRANKS'        : ,}
    """
    sim.solver.max_iterations     = parameters['MAX_ITERATIONS']
    sim.solver.absolute_tolerance = 0
    sim.solver.relative_tolerance = parameters['REL_TOLERANCE']
    sim.solver.preconditioner     = True
    sim.validate()
    
    # Adding stations has to happen here because 
    # it is based on MAX_ITERATIONS parameter 
    # of the sim.solver config
    if len(stations)>0:
        add_stations(sim         = sim, 
                     stations    = stations, 
                     output_file = pout)
    
    sn.api.run(
        input_file    = sim,
        site_name     = parameters['SITENAME'],
        output_folder = directory,
        overwrite     = True,
        ranks         = parameters['NRANKS'],
        wall_time_in_seconds = 3600 # required only if you run the simulation on a cluster
    )
    
    return 'Simulation finished'

def read_solution(filepath, field_prefix):
    assert os.path.exists(filepath) and filepath.suffix == '.h5' 
    output_dict = {}
    sol_mesh = sn.UnstructuredMesh.from_h5(filepath)
    for key in sol_mesh.elemental_fields.keys():
        if key.startswith(field_prefix):
            output_dict[key] = sol_mesh.elemental_fields[key]
    return output_dict

def read_receiver_solution(filename, field = 'phi', acoustic_or_elastic='ACOUSTIC'):
    assert acoustic_or_elastic in ['ACOUSTIC', 'ELASTIC']
    with h5py.File(filename, 'r') as file:
        indices     = np.argsort(file[f'receiver_ids_{acoustic_or_elastic}_point'])
        data        = file['point'][field][:,:,-1][indices]
        coordinates = file[f'coordinates_{acoustic_or_elastic}_point'][:][indices]
    return data, coordinates



# + deletable=false
parameters = Parameters()
paths = Paths(f'full_alpha_minimal_{lat_step}x{lon_step}')
paths.setup_folder()

parameters['GRAVITY']          = True
parameters['TENSOR_ORDER']     = 2
parameters['NEX']              = 24
parameters['NELEM_VERTICAL']   = [2,2]
parameters['REL_TOLERANCE']    = 1e-10
parameters['REFINEMENT_LEVEL'] = 1
parameters['ONED_MODEL']       = 'prem_iso_one_crust'#'../input/1d_models/kemRper23_smks2.bm'
print(parameters, paths)

parameters['SITENAME'] = 'local'
parameters['NRANKS']   = 100

# + deletable=false
mesh = create_mesh(paths, parameters)
find_sideset(mesh, CMB_R, tolerance = 1e3)



# + deletable=false
gravity_mesh = prepare_gravity_mesh(mesh);
gravity_mesh.write_h5(paths.gravity_mesh_file)

gravity_sim = gravity_config(mfile         = paths.gravity_mesh_file, 
                             vout          = paths.gravity_solution,
                             polynom_order = parameters['TENSOR_ORDER'],
                             oned_model    = parameters['ONED_MODEL'],)

print(gravity_sim.get_dictionary()['physics'])

run_simulation(   sim       = gravity_sim,
                  directory = paths.gravity_solution.parent,
                  parameters= parameters)
gravity_solution = read_solution(paths.gravity_solution, 'gradient')

for comp in ['X', 'Y', 'Z']:
    mesh.elemental_fields[f'GRAD_PHI_{comp}'] = gravity_solution[f'gradient-of-phi_{comp}']
g = np.linalg.norm([mesh.elemental_fields[f'GRAD_PHI_{comp}'] for comp in ['X', 'Y', 'Z']], axis=0)
mesh.attach_field('g', g)

# + deletable=false
elastos_mesh = mesh
elastos_mesh.elemental_fields['fluid'] *= 0
elastos_mesh.attach_field('DN_RHO_G', np.zeros((mesh.nelem, mesh.nodes_per_element)))

# +
parameters['SITENAME'] = 'tides_alpha'

for cpl_comp in complex_components:
    #adding Neumann BC to the mesh - it an elemental_field which is non-zero only at the surface. 'tidal_elevation_re' field was added to the mesh by Martin's meshing routine:
    M2_load = (-1) * elastos_mesh.element_nodal_fields[f'tidal_elevation_{cpl_comp}'] * 1030. #* elastos_mesh.elemental_fields['g'] 
    elastos_mesh.attach_field('NEUMANN', M2_load)
    elastos_mesh.write_h5(paths.elastos_mesh_file) 
    
    elastos_sim = elastic_config( mfile           = str(paths['elastos_mesh_file']), 
                                  vout            = str(paths[f'vol_solution_{cpl_comp}']), 
                                  polynom_order   = parameters['TENSOR_ORDER'],  
                                  cowling_or_full ='full',
                                )
    
    print(elastos_sim.get_dictionary()['physics'])
    
    _ = run_simulation(   sim       = elastos_sim,
                          directory = str(paths[f'vol_solution_{cpl_comp}'].parent),
                          parameters= parameters,
                          stations  = station_grid.sideset_points_displacement.data.ravel(),
                          pout      = str(paths[f'point_solution_{cpl_comp}'].name)
                      )
# +
for cpl_comp in complex_components:
    point_solution, _ = read_receiver_solution(filename = paths[f'point_solution_{cpl_comp}'],
                                                                    field = 'displacement',
                                                                    acoustic_or_elastic='ELASTIC')
    station_grid = station_grid.assign_coords(coords={'VNE': ['V','N','E']})
    shape = station_grid.lat.shape + station_grid.lon.shape + (3,)
    station_grid = station_grid.assign({f'displacement_{cpl_comp}': (['lat','lon','VNE'], point_solution.reshape(shape))})

station_grid = station_grid.assign(displacement_re_magnitude=(['lat','lon'],\
                                                              np.linalg.norm(station_grid.displacement_re,axis=-1) ))
# -

if 'sideset_points_displacement' in station_grid:
    del station_grid['sideset_points_displacement']
station_grid.to_netcdf(str(paths.point_solution_table))
print(f'station grid was saved to {paths.point_solution_table}')



