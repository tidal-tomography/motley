import pygmt
import numpy as np

def plot_DataArray(fig, da, cb_name='', cb_units = 'm', 
                   title="", projection = "R12c", region='d', series=[], 
                   landmask = '', oceanmask = '', coastline_color = '#FFFFFF', 
                   cmap='polar', reverse_cmap=False,
                   grid=False, colorbar = True, **kwargs):
    """
    Adds figure based on data from dataarray to a pygmt.Figure object
    See available colormaps here: https://docs.generic-mapping-tools.org/dev/cookbook/cpts.html
    Input:
    ---------
    da       - xarray.DataArray to be plotted
    cb_name  - caption under the colorbar 
    cb_units - caption at the colorbar. Usually used for marking units (default: m).
    series   - minimum and maximum values for the colormap. If not provided, min/max values of the dataarray are used
    landmask - fill up the contitental area with the provided colorcode, e.g. '#FFFFFF' for white
    
    Optional:
    ---------
    frame_gmt_code   - list of the GMT frame options, overwrites the 'title' and 'grid' parameters
    FONT_TITLE       - Font to use when plotting titles over graphs [default is theme dependent]. Choose auto for automatic scaling with plot size. (from https://docs.generic-mapping-tools.org/dev/gmt.conf.html#term-FONT_TITLE)
    MAP_TITLE_OFFSET - Distance from top of axis annotations (or axis label, if present) to base of plot title [default is theme dependent]. Choose auto for automatic scaling with plot size.
    """
    
    if kwargs:
        for key in ['FONT_TITLE', 'MAP_TITLE_OFFSET']:
            if key in kwargs.keys(): pygmt.config(**{key: kwargs[key]})
    
    # Make colormap and apply it to the drawing
    if len(series) < 2: 
        series=[np.min(da.data), np.max(da.data)]
        
    pygmt.makecpt(series=series, cmap=cmap, reverse=bool(reverse_cmap))
    if 'frame_gmt_code' in kwargs.keys():
        frame_gmt_code = kwargs['frame_gmt_code']
    else:
        frame_gmt_code = ["af"]
        if grid: frame_gmt_code[0] += 'g' #Draw geographical grid
        if title: frame_gmt_code.append(f'+t"{title}"')
        
    fig.grdimage(da, region=region, frame=frame_gmt_code, projection=projection, nan_transparent = True)

    coastparams = dict(shorelines=f"0.5p,{coastline_color}", region=region,  projection=projection)
    if landmask:  coastparams['land']   = landmask
    if oceanmask: coastparams['water']  = oceanmask
    fig.coast(**coastparams)
     
    if len(cb_name)>0: xcb = f'x+l"{cb_name}"'
    else: xcb = ""
    if colorbar: fig.colorbar(frame=[ xcb, f'y+l"{cb_units}"'], projection=projection,) 
    return fig