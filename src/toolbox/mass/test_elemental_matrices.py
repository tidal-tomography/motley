#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Test for the mapping of lagrangian basis derivatives into a tensor basis in 2D
and 3D

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2019
:license:
    None
'''
import numpy as np
from elemental_matrices import get_Dn, get_jacobian, get_integration_weights, \
        get_jacobian_determinant, get_mass_matrix
import pytest

from salvus.mesh.structured_grid_2D import StructuredGrid2D
from salvus.mesh.structured_grid_3D import StructuredGrid3D
from salvus.mesh.unstructured_mesh import UnstructuredMesh

import sys
sys.path.append('../1D_sem/')

from quadrature_points_weights import \
    gauss_lobatto_legendre_quadruature_points_weights as points_weights  # NoQa


def test_get_Dn():
    # regression tests

    p, w = points_weights(2)
    Dn = get_Dn(p, 2)

    Dn0_ref = np.array([-0.5, 0.5, 0., 0., -0.5, 0.5, 0., 0., 0., 0., -0.5,
                        0.5, 0., 0., -0.5, 0.5])
    Dn1_ref = np.array([-0.5, 0., 0.5, 0., 0., -0.5, 0., 0.5, -0.5, 0., 0.5,
                        0., 0., -0.5, 0., 0.5])

    np.testing.assert_allclose(Dn[0].flatten(), Dn0_ref, atol=1e-15)
    np.testing.assert_allclose(Dn[1].flatten(), Dn1_ref, atol=1e-15)

    Dn = get_Dn(p, 3)

    Dn0_ref = np.array([-0.5, 0.5, 0., 0., 0., 0., 0., 0., -0.5, 0.5, 0., 0.,
                        0., 0., 0., 0., 0., 0., -0.5, 0.5, 0., 0., 0., 0., 0.,
                        0., -0.5, 0.5, 0., 0., 0., 0., 0., 0., 0., 0., -0.5,
                        0.5, 0., 0., 0., 0., 0., 0., -0.5, 0.5, 0., 0., 0., 0.,
                        0., 0., 0., 0., -0.5, 0.5, 0., 0., 0., 0., 0., 0.,
                        -0.5, 0.5])
    Dn1_ref = np.array([-0.5, 0., 0.5, 0., 0., 0., 0., 0., 0., -0.5, 0., 0.5,
                        0., 0., 0., 0., -0.5, 0., 0.5, 0., 0., 0., 0., 0., 0.,
                        -0.5, 0., 0.5, 0., 0., 0., 0., 0., 0., 0., 0., -0.5,
                        0., 0.5, 0., 0., 0., 0., 0., 0., -0.5, 0., 0.5, 0., 0.,
                        0., 0., -0.5, 0., 0.5, 0., 0., 0., 0., 0., 0., -0.5,
                        0., 0.5])
    Dn2_ref = np.array([-0.5, 0., 0., 0., 0.5, 0., 0., 0., 0., -0.5, 0., 0.,
                        0., 0.5, 0., 0., 0., 0., -0.5, 0., 0., 0., 0.5, 0., 0.,
                        0., 0., -0.5, 0., 0., 0., 0.5, -0.5, 0., 0., 0., 0.5,
                        0., 0., 0., 0., -0.5, 0., 0., 0., 0.5, 0., 0., 0., 0.,
                        -0.5, 0., 0., 0., 0.5, 0., 0., 0., 0., -0.5, 0., 0.,
                        0., 0.5])

    np.testing.assert_allclose(Dn[0].flatten(), Dn0_ref, atol=1e-15)
    np.testing.assert_allclose(Dn[1].flatten(), Dn1_ref, atol=1e-15)
    np.testing.assert_allclose(Dn[2].flatten(), Dn2_ref, atol=1e-15)


@pytest.mark.parametrize("order", [3, 4, 5, 6, 7, 8, 9, 10, 15, 20])
def test_get_Dn_analytical(order):
    p, w = points_weights(order+1)

    # 2D derivative test
    mp = np.array([[-1., -1.], [1., -1], [-1., 1], [1., 1.]])
    mc = np.array([[0, 1, 2, 3]])
    m = UnstructuredMesh(mp, mc)
    m.change_tensor_order(order)

    x, y = m.points[m.connectivity[0]].T

    # analytical function and derivatives
    f = 2 * x + y ** 2 + x * y + x ** order + y ** order
    dxf = 2 * np.ones_like(x) + y + order * x ** (order - 1)
    dyf = 2 * y + x + order * y ** (order - 1)

    Dx, Dy = get_Dn(p, 2)

    np.testing.assert_allclose(dxf, np.dot(Dx, f), atol=1e-14)
    np.testing.assert_allclose(dyf, np.dot(Dy, f), atol=1e-14)

    # 3D derivative test
    mp = np.array(
        [[-1., -1., -1.], [1., -1, -1.], [-1., 1, -1.], [1., 1., -1.],
         [-1., -1., 1.], [1., -1, 1.], [-1., 1, 1.], [1., 1., 1.]])
    mc = np.array([[0, 1, 2, 3, 4, 5, 6, 7]])
    m = UnstructuredMesh(mp, mc)
    m.change_tensor_order(order)

    x, y, z = m.points[m.connectivity[0]].T

    # analytical function and derivatives
    f = 2 * x + y ** 2 + x * y + z ** 3 + x ** order + y ** order + z ** order
    dxf = 2 * np.ones_like(x) + y + order * x ** (order - 1)
    dyf = 2 * y + x + order * y ** (order - 1)
    dzf = 3 * z ** 2 + order * z ** (order - 1)

    Dx, Dy, Dz = get_Dn(p, 3)

    np.testing.assert_allclose(dxf, np.dot(Dx, f), atol=1e-13)
    np.testing.assert_allclose(dyf, np.dot(Dy, f), atol=1e-13)
    np.testing.assert_allclose(dzf, np.dot(Dz, f), atol=1e-13)


@pytest.mark.parametrize("m", [
    StructuredGrid2D.rectangle(3, 7).get_unstructured_mesh(),
    StructuredGrid3D.cube(2, 4, 7).get_unstructured_mesh()])
def test_get_jacobian(m):

    # randomly perturb nodes
    m.points += np.random.randn(m.points.size).reshape(m.points.shape) * 1e-2
    j = get_jacobian(m)

    # reference solution from salvus_mesh
    j_ref = m._jacobian_at_nodes()
    np.testing.assert_allclose(j, j_ref, atol=1e-15)

    # test for some nodes on higer order (as we converted linearly, after
    # perturbing the corner points, it should be equal on the corners)
    m.change_tensor_order(4)
    j = get_jacobian(m)

    np.testing.assert_allclose(j[:, 0], j_ref[:, 0], atol=1e-15)
    np.testing.assert_allclose(j[:, -1], j_ref[:, -1], atol=1e-15)
    np.testing.assert_allclose(j[:, 4], j_ref[:, 1], atol=0e-15)
    np.testing.assert_allclose(j[:, 24], j_ref[:, 3], atol=0e-15)


@pytest.mark.parametrize("m", [
    StructuredGrid2D.rectangle(3, 2).get_unstructured_mesh(),
    StructuredGrid3D.cube(2, 4, 7).get_unstructured_mesh(),
    ])
def test_get_jacobian_determinant(m):
    # randomly perturb nodes
    m.points += np.random.randn(m.points.size).reshape(m.points.shape) * 1e-2

    detJ = get_jacobian_determinant(m)
    detJ_ref = np.linalg.det(get_jacobian(m))

    np.testing.assert_allclose(detJ, detJ_ref, atol=1e-15)


@pytest.mark.parametrize("order", [3]) #, 4, 5, 6, 7, 8, 9, 10, 15, 20])
def test_get_integration_weights(order):
    p, w = points_weights(order+1)

    # 2D integration test
    mp = np.array([[-1., -1.], [1., -1], [-1., 1], [1., 1.]])
    mc = np.array([[0, 1, 2, 3]])
    m = UnstructuredMesh(mp, mc)
    m.change_tensor_order(order)

    x, y = m.points[m.connectivity[0]].T

    # analytical function and integral
    f = x ** order + y ** (2 * order - 2)
    n1 = order + 1
    n2 = 2 * order - 1
    If = (1 ** n1 / n1 - (-1) ** n1 / n1) * 2
    If += (1 ** n2 / n2 - (-1) ** n2 / n2) * 2

    Wn = get_integration_weights(w, 2)
    np.testing.assert_allclose(np.dot(Wn, f), If, atol=1e-15)

    # 3D integration test
    mp = np.array(
        [[-1., -1., -1.], [1., -1, -1.], [-1., 1, -1.], [1., 1., -1.],
         [-1., -1., 1.], [1., -1, 1.], [-1., 1, 1.], [1., 1., 1.]])
    mc = np.array([[0, 1, 2, 3, 4, 5, 6, 7]])
    m = UnstructuredMesh(mp, mc)
    m.change_tensor_order(order)

    x, y, z = m.points[m.connectivity[0]].T

    # analytical function and integral
    f = x ** order + y ** (2 * order - 2) + z
    n1 = order + 1
    n2 = 2 * order - 1
    If = (1 ** n1 / n1 - (-1) ** n1 / n1) * 4
    If += (1 ** n2 / n2 - (-1) ** n2 / n2) * 4
    If += (1 ** 2 / 2 - (-1) ** 2 / 2) * 4

    Wn = get_integration_weights(w, 3)
    np.testing.assert_allclose(np.dot(Wn, f), If, atol=1e-15)


@pytest.mark.parametrize("order", [3, 4, 5, 6, 7, 8, 9, 10, 15, 20])
def test_get_mass_matrix(order):

    # 2D integration test
    sg = StructuredGrid2D.rectangle(3, 5, min_x=-1., min_y=-1.)
    m = sg.get_unstructured_mesh()
    m.change_tensor_order(order)

    x, y = m.points[m.connectivity].T

    # analytical function and integral
    f = x ** order + y ** (2 * order - 2)
    n1 = order + 1
    n2 = 2 * order - 1
    If = (1 ** n1 / n1 - (-1) ** n1 / n1) * 2
    If += (1 ** n2 / n2 - (-1) ** n2 / n2) * 2

    M = get_mass_matrix(m)
    np.testing.assert_allclose((M * f.T).sum(), If, atol=1e-15)

    # 3D integration test
    sg = StructuredGrid3D.cube(3, 5, 2, min_x=-1., min_y=-1., min_z=-1.)
    m = sg.get_unstructured_mesh()
    m.change_tensor_order(order)

    x, y, z = m.points[m.connectivity].T

    # analytical function and integral
    f = x ** order + y ** (2 * order - 2) + z
    n1 = order + 1
    n2 = 2 * order - 1
    If = (1 ** n1 / n1 - (-1) ** n1 / n1) * 4
    If += (1 ** n2 / n2 - (-1) ** n2 / n2) * 4
    If += (1 ** 2 / 2 - (-1) ** 2 / 2) * 4

    M = get_mass_matrix(m)
    np.testing.assert_allclose((M * f.T).sum(), If, atol=1e-15)
