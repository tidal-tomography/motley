import numpy as np
import os
import h5py

import salvus.namespace as sn

def read_solution(filepath, field_prefix):
    assert os.path.exists(filepath) and filepath.suffix == '.h5' 
    output_dict = {}
    sol_mesh = sn.UnstructuredMesh.from_h5(filepath)
    for key in sol_mesh.elemental_fields.keys():
        if key.startswith(field_prefix):
            output_dict[key] = sol_mesh.elemental_fields[key]
    return output_dict

def read_receiver_solution(filename, field = 'phi', acoustic_or_elastic='ACOUSTIC'):
    assert acoustic_or_elastic in ['ACOUSTIC', 'ELASTIC']
    with h5py.File(filename, 'r') as file:
        indices     = np.argsort(file[f'receiver_ids_{acoustic_or_elastic}_point'])
        data        = file['point'][field][:,:,-1][indices]
        coordinates = file[f'coordinates_{acoustic_or_elastic}_point'][:][indices]
    return data, coordinates