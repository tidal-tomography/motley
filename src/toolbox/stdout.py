import os
import numpy as np
import matplotlib.pyplot as plt


class Stdout:
    header = ['iteration', '$r^2$', 'Relative error$^2$', 'Precond. residual$^2$', 'Rel. error^2']
    @staticmethod
    def get_string_part(string, key_start, key_end):
        """Returns indeces of a string part within the string"""
        index_start = string.find(key_start)
        index_end   = index_start + string[index_start:].find(key_end)
        return index_start, index_end

    def read_stdout_file(self, filename):
        def format_line(line):
            return np.array(line.strip().split(), dtype=float)
        with open(filename) as file:
            fileread        = file.read()
            ind_st, ind_end = self.get_string_part(fileread, 'it\t', '\n\n')
            temp            = fileread[ind_st:ind_end].splitlines()[2:-2]
            if temp[1].startswith('-----'): temp.pop(1)
        return np.array([format_line(line) for line in temp])

    def read_parameters(self, filename, keys = ['Floating point size', 'Running on', 'Job started']):
        params = {}
        with open(filename) as file:
            fileread = file.read()
            for key in keys:
                ind_st, ind_end = self.get_string_part(fileread, key, '\n')
                params[key] = fileread[ind_st:ind_end].lstrip(key).strip()
        return params
    
    def __init__(self, filename):
        assert os.path.exists(filename)
        self.filename   = filename
        self.data       = self.read_stdout_file(filename)
        self.parameters = self.read_parameters(filename)
    
    def plot(self, index, label=''):
        fig = plt.figure()
        print(f'Plotting {self.header[index]}')
        plt.semilogy(self.data[1:,index])
        plt.xlabel(self.header[0])
        plt.ylabel(self.header[index])
        min_value = np.min(self.data[:,index])
        max_value = np.max(self.data[:,index])
        min_value_iter = int(np.where(self.data[:,index] == min_value)[0])
        plt.vlines(min_value_iter, min_value, max_value, linestyles='--', color = 'k', linewidth=0.5, label=label)
        plt.text(min_value_iter*1.05, max_value,label)
        
        #plt.text(min_value_iter*1.05, max_value, f'Minimal value: {min_value} at iteration #{min_value_iter}')