import numpy as np

def find_sideset(mesh, radius, tolerance=30., sideset_name='r0'):
    '''
    That's a function which is attempting to find the CMB surface and add it to the mesh as a sideset. 
    If you need to find a different surface, you need to play around with the dist() function. 
    '''
    if sideset_name in mesh.side_sets.keys(): print(f'"{sideset_name}" is already attached'); return 0
    def dist(x):
        return np.abs(radius - np.linalg.norm(x, axis=-1)) 
    mesh.find_side_sets_generic(sideset_name, dist, tolerance=tolerance)
    print(f'"{sideset_name}" has been found') if sideset_name in mesh.side_sets.keys()\
    else print('Resolution is not fine enough to find the surface. Increase the tolerance')
    
def get_point_index(i,j,k, ppedge):
    # outputs local number of the point in 3D: i, j, k - eps1, eps2, eps3 local coordinates
    # ppedge: number of points per element edge
    return i + j*ppedge + k*ppedge**2     
    
    
def get_nnodes_per_facet(tensor_order):
    return (tensor_order + 1)**2

def get_facet_nodes(nfacet, tensor_order=2):
    """outputs array consisting the nodal points indeces of the surface"""

    nn_ed    = (tensor_order + 1) #number of nodes per edge
    nn_f     =  nn_ed**2          #number of nodes per facet
    nn_el    =  nn_ed**3          #number of nodes per element
    
    facet2, facet3  = [],[]
    for i in range(nn_ed):
        st2     = i * nn_f                   #starting index facet2
        facet2 += list(range(st2, st2+nn_ed))
        st3     = i * nn_f + (nn_f - nn_ed)  #starting index facet3
        facet3 += list(range(st3, st3+nn_ed))
    
    facets = {'0': list( range(                0, nn_f )       ),
              '1': list( range( nn_f*(nn_ed - 1), nn_el)       ),
              '2':                                        facet2,
              '3':                                        facet3,
              '4': list( range(      (nn_ed - 1), nn_el, nn_ed)),
              '5': list( range(                0, nn_el, nn_ed)),}
        
    if hasattr(nfacet, '__iter__'):
        return np.array([facets[f'{face}'] for face in nfacet])
    else:
        return facets[str(nfacet)]
    
def get_surface_mask(mesh, sideset='r1'):
    nodes_per_facet = get_nnodes_per_facet(mesh.shape_order)

    surface_elem  = np.repeat(mesh.side_sets[sideset][0], nodes_per_facet)
    surface_nodes = get_facet_nodes(mesh.side_sets[sideset][1], tensor_order=mesh.shape_order).flatten()

    return tuple([surface_elem, surface_nodes])


def tell_me_about_mesh(mesh):
    EARTH_R = 6371e3
    separator = '-'*30 + '\n'
    nodal_radii = np.linalg.norm(mesh.points[mesh.connectivity], axis=-1)
    
    info = ''
    info += f'Number of elements: {mesh.nelem}\n' 
    info += f'Order of the shape function: {mesh.shape_order}\n'
    info += f'Farthest nodal point: {np.max(nodal_radii)} m, {np.max(nodal_radii)/EARTH_R} x R_Earth\n'
    
    
    maxkeylen = np.max([len(key) for key in mesh.elemental_fields.keys()])
    info += f'Elemental fields,   value range,   field shape:\n'
    info += separator
    for key in mesh.elemental_fields.keys():
        field = mesh.elemental_fields[key]
        info += f'{key.ljust(maxkeylen+1)}: {float(np.min(field)):14.8} - {float(np.max(field)):<14.8} {field.shape}\n'
    info += separator
    
    info += 'Side sets:\n'
    info += separator
    for side in mesh.side_sets:
        surface_mask = get_surface_mask(mesh, side)
        sidemeanrad  = np.mean(nodal_radii[surface_mask])
        info += f'{side}: mean r: {sidemeanrad:14.8} m, {sidemeanrad/EARTH_R:14.8} x R_Earth\n'
        
    print(info)
    return info

def get_layer_extend(mesh, nl):
    """Outputs the smallest and largest nodal radius within the layer"""
    lmask = np.array(mesh.elemental_fields['layer'] == nl, dtype=bool)
    lr    = np.linalg.norm(mesh.points[mesh.connectivity][lmask], axis=-1)
    print(f'Layer #{nl}: {np.min(lr):10.8} - {np.max(lr):<10.8}m')
    return (np.min(lr), np.max(lr))

def get_layer_mask(mesh, nlayer:list = [0,1]):
    # MUST RETURN BOOL ARRAY
    mask = np.zeros_like(mesh.elemental_fields['layer'], dtype=bool)
    for nlayer in range(min(nlayer), max(nlayer) + 1):
        print(nlayer)
        mask[mesh.elemental_fields['layer'] == nlayer] = True
    lr   = np.linalg.norm(mesh.points[mesh.connectivity][mask], axis=-1)
    print(f'Mask extend: {np.min(lr):10.8} - {np.max(lr):<10.8}m')
    return mask

    
def get_rotation_matrix(lat, lon):
    return np.array([[ np.cos(lat)*np.cos(lon),  np.cos(lat)*np.sin(lon), np.sin(lat)],
                     [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
                     [-np.sin(lon)            ,              np.cos(lon),           0]])



def get_element_rotation_matrix(lat, lon):
    temp       = np.array([[  np.cos(lat)*np.cos(lon),  np.cos(lat)*np.sin(lon),        np.sin(lat)], 
                           [ -np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon),        np.cos(lat)],
                           [ -np.sin(lon)            ,              np.cos(lon), np.zeros_like(lon)]])
    rotat_matr        = np.zeros((len(lat), 3, 3), dtype=np.float64)
    for i in range(3):
        for j in range(3):
            rotat_matr[:,i,j] = temp[i,j]
    return rotat_matr

def integrate_over_surface(mesh, fieldname):
    """
    If you apply it to a non-spherical surface, you need to figure out how to find surficial nodes
    """
    J = get_jacobian(mesh)

    surface_elems = mesh.side_sets['r1'][0]
    
    tol = 1.
    sideset_nodes = list(mesh.get_side_set_nodes('r1'))
    # the assumption here is that all element have the save nodal point numbers at the surface
    surface_nodes = np.where(np.isin(mesh.connectivity[mesh.side_sets['r1'][0]][0], sideset_nodes))[0]

    nlggpoints    = mesh.shape_order + 1
    crds, weights = gauss_lobatto_legendre_quadruature_points_weights(nlggpoints)
    summ  = 0
    field = mesh.elemental_fields[fieldname]
    
    for elem in surface_elems:
        for n, nnode in enumerate(surface_nodes):    
            i = n%nlggpoints
            j = int(n/nlggpoints)
            w = weights[i] * weights[j]
            summ += field[elem][nnode] *w *area(J[elem][nnode], 1)
            
    return summ

def det_minor(matrix, i, j):
    return np.linalg.det(np.delete(np.delete(matrix, j, axis=0), i, axis=1))

def area(matrix, epsilon):
    vector_product = np.zeros(3)
    for i in range(3):
        vector_product[i] = det_minor(matrix, i, epsilon)
    return np.linalg.norm(vector_product)
