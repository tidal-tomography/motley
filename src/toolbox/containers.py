import os
from pathlib import Path as plPath

class LocalContainer:
    def __init__(self): pass
    def __repr__(self):
        output_string = ''
        for key in self.__dict__.keys():
            output_string += f'{key:<20} : {self.__dict__[key]}\n'
        return output_string
    def __contains__(self, item)           : return item in self.__dict__.keys()
    def __setitem__ (self, itemname, value): self.__dict__[itemname] = value
    def __getitem__ (self, itemname)       : return self.__dict__[itemname]
    def __delitem__ (self, itemname)       : pass
    def __iter__    (self)                 : pass
    def __len__     (self)                 : pass

# class Path():
#     def __init__(self, path):
#         self.path = path
#     def setup_folder(self):
#         os.makedirs(self.path, exist_ok=True)

class Paths(LocalContainer):
    def __init__(self, scenario=''):
        if len(scenario)>0: scenario += '/'
        inputdir    = plPath('../input/')
        outputdir   = plPath(f'../output/{scenario}')
        solutiondir = outputdir / 'solution'
        self.loading_file         = inputdir    / 'load_models/tidal_load_m2_10800_new_grad_new_mask.nc'
        self.station_network      = inputdir    / 'stations/global_1deg.csv'
        self.surface_topo         = inputdir    / 'topo/topography_earth2014_egm2008_lmax_256.nc'
        self.moho_topo            = inputdir    / 'topo/moho_topography_crust_1_0_egm2008.nc'
        self.mesh_file            = outputdir   / 'mesh/mesh.h5'
        self.gravity_mesh_file    = outputdir   / 'mesh/gravity_mesh.h5'
        self.elastos_mesh_file    = outputdir   / 'mesh/elastos_mesh.h5'
        self.vol_solution_re      = solutiondir / 'vol_solution_re.h5'
        self.vol_solution_im      = solutiondir / 'vol_solution_im.h5'
        self.point_solution_re    = solutiondir / 'point_solution_re.h5'
        self.point_solution_im    = solutiondir / 'point_solution_im.h5'
        self.point_solution_table = solutiondir / 'point_solution.nc'
        self.gravity_solution     = solutiondir / 'gravity_solution.h5'
        self.gravity_psolution    = solutiondir / 'gravity_solution_point.h5'
        self.pictures             = outputdir   / 'pics/'
        self.trash                = outputdir   / 'trash/'

    def setup_folder(self):
        for key in self.__dict__.keys():
            if not self.__dict__[key].exists():
                if len(self.__dict__[key].suffix) > 0:
                    self.__dict__[key].parent.mkdir(parents=True, exist_ok=True)
                else:
                    self.__dict__[key].mkdir(parents=True, exist_ok=True)
            
class Parameters(LocalContainer):
    def __init__(self):
        #meshing
        self.NEX                  = 24
        self.TENSOR_ORDER         = 2
        self.NELEM_VERTICAL       = [2,2]
        self.ONED_MODEL           = 'prem_iso_one_crust'
        self.TOPOGRAPHY           = False
        self.MOHO_TOPO            = False
        #surface loading
        self.LMAX                 = 256
        self.REFINEMENT_LEVEL     = 0
        #simulation
        self.MAX_ITERATIONS       = 10000
        self.REL_TOLERANCE        = 1e-10
        self.SITENAME             = 'local'
        self.NRANKS               = 10
        self.GRAVITY              = False
        self.NELEM_BUFFER_OUTER   = 20
