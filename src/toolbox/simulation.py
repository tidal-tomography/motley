import numpy as np
import os
from .constants import G, EARTH_MASS, EARTH_R

from salvus.flow import api, simple_config
from salvus.mesh import models_1D
import salvus.namespace as sn

def get_theoretical_potential(radius, model_1d):
    try:
        model_1d                 = models_1D.model.built_in(model_1d)
    except FileNotFoundError:
        model_1d                 = models_1D.model.read(model_1d)
    outer_boundary_rel_radius = radius / EARTH_R
    outer_boundary_potential  = model_1d.get_gravitational_potential(radius=np.array([outer_boundary_rel_radius]))[0]
    point_mass_potential      = G * EARTH_MASS / (outer_boundary_rel_radius * EARTH_R) * (-1)
    print(f'External Dirichlet boundary:\n\tRadius: {outer_boundary_rel_radius} x R_Earth \n\t1D gravity potential: {outer_boundary_potential} J/kg\n\tPoint-mass potential: {point_mass_potential} J/kg')
    return outer_boundary_potential 

def gravity_config( mfile:str, vout:str, polynom_order:int, oned_model:str, stations:list=[], pout:str=''):
    '''
        Configure a poisson simulation. 
            mfile    - mesh file path
            vout     - volumetric output file path
            pout     - pointwise output path
            stations - list of SideSetPoint3D objects
            cowling_or_full - cowling or full gravity
    requires M0, M1, RHS, elemental fields attached to the mesh. 
    Depending on the type of the used boundary condition, might also require NEUMANN field and additional sidesets.
    "fluid" elemental field must be set to ones
    '''
    mesh                                                  = sn.UnstructuredMesh.from_h5(mfile)
    sim                                                   = sn.simple_config.simulation.Poisson(mesh=mesh)

    sim.domain.polynomial_order                           = polynom_order
    sim.physics.poisson_equation.right_hand_side.filename = str(mfile)
    sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
    sim.physics.poisson_equation.right_hand_side.field    = "RHO"

    outer_boundary_potential = get_theoretical_potential(np.max( np.linalg.norm(mesh.points, axis=-1) ) , model_1d = oned_model)
    boundaries               = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"], value = float(outer_boundary_potential))
    sim.add_boundary_conditions(boundaries)
    
    sim.physics.poisson_equation.solution.filename   = os.path.basename(vout)
    sim.physics.poisson_equation.solution.fields     = ["solution", "gradient-of-phi", 'right-hand-side']
    
    return sim
        
def elastic_config( mfile:str, vout:str, polynom_order:int, cowling_or_full='cowling'):
    """
        Configure an elastic simulation. 
            mfile    - mesh file path
            vout     - volumetric output file path
            pout     - pointwise output path
            stations - list of SideSetPoint3D objects
            cowling_or_full - cowling or full gravity
    """
    
    mesh                                             = mfile
    sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
    sim.domain.polynomial_order                      = polynom_order

    # Cowling approximation ('cowling') or full gravity ('full')
    sim.physics.elastostatic_equation.gravity                  = cowling_or_full
    sim.physics.elastostatic_equation.right_hand_side.filename = mfile
    sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
    #Here we put the body force field. In surface loading problems it's zero everythere. 'zeros' is an elemental field of the mesh that we added before.
    if cowling_or_full == 'cowling':
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros"]*3
    elif cowling_or_full == 'full':
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros"]*4
    else:
        raise ValueError('allowed simulation types are "cowling" and "full"')
        
    if len(vout)>0: 
        sim.physics.elastostatic_equation.solution.filename        = os.path.basename(vout)
        sim.physics.elastostatic_equation.solution.fields          = ["solution", 'gradient-of-displacement']

#     if    cowling_or_full=='full'   : dirichlet_sidesets = ['r0', 'r2']
#     elif  cowling_or_full=='cowling': dirichlet_sidesets = ['r0']
#     dirichlet_bc = simple_config.boundary.HomogeneousDirichlet( side_sets=dirichlet_sidesets )
#     sim.add_boundary_conditions(dirichlet_bc)
    if cowling_or_full == 'full':
        dirichlet_sidesets = ['r2']
        dirichlet_bc = simple_config.boundary.HomogeneousDirichlet( side_sets=dirichlet_sidesets )
        sim.add_boundary_conditions(dirichlet_bc)
    
    neumann_bc   = simple_config.boundary.Neumann( side_sets=["r1"] )
    sim.add_boundary_conditions(neumann_bc)

    return sim

def add_stations(sim, stations, output_file):
    """Adds stations to a simulation object. 
    output_file should include only the name of the file, not the path to it
    """
    
    if hasattr( stations, '__iter__') * (len(stations) > 0):
        sim.add_receivers(stations)
        sim.output.point_data.format                          = "hdf5"
        sim.output.point_data.filename                        = output_file
        sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations 
        print(f'{len(stations)} stations were added to the simulation')

def run_simulation(sim, directory:str, parameters:dict, stations=[], pout=''):
    """
    Run a simulation. 
            vout       - volumetric output file path
            parameters - simulation parameters
       parameters must be a dictionary including the following keys:
    {'MAX_ITERATIONS': ,
     'REL_TOLERANCE' : ,
     'SITENAME'      : ,
     'NRANKS'        : ,}
    """
    sim.solver.max_iterations     = parameters['MAX_ITERATIONS']
    sim.solver.absolute_tolerance = 0
    sim.solver.relative_tolerance = parameters['REL_TOLERANCE']
    sim.solver.preconditioner     = True
    sim.validate()
    
    # Adding stations has to happen here because 
    # it is based on MAX_ITERATIONS parameter 
    # of the sim.solver config
    if len(stations)>0:
        add_stations(sim         = sim, 
                     stations    = stations, 
                     output_file = pout)
    
    sn.api.run(
        input_file    = sim,
        site_name     = parameters['SITENAME'],
        output_folder = directory,
        overwrite     = True,
        ranks         = parameters['NRANKS'],
        wall_time_in_seconds = 3600 # required only if you run the simulation on a cluster
    )
    
    return 'Simulation finished'