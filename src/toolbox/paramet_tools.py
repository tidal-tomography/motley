import numpy as np

# Complex numbers:

def compl2polar(real, imag):
    absv  = np.linalg.norm([real, imag])
    phase = np.arctan2(imag, real)
    phase = phase if imag > 0 else 2*np.pi + phase
    return absv, phase
def polar2compl(amp, phase):
    r = amp*np.cos(phase)
    i = amp*np.sin(phase)
    return r, i

#Material parameters
def get_mu(vs, rho):
    return vs**2 *rho
def get_lambda(vp, vs, rho):
    return rho * (vp**2 - 2*vs**2)    
def voigt_average(vpv, vph, vsv, vsh):
    # [Panning & Romanowicz, 2006]
    vp = ((  vpv**2 + 4*vph**2) /5.)**0.5
    vs = ((2*vsv**2 +   vsh**2) /3.)**0.5
    return vp, vs

def change_parametrization(mesh):
    '''
    from vp, vs to lambda, mu
    '''
    VP  = mesh.elemental_fields['VP']
    VS  = mesh.elemental_fields['VS']
    RHO = mesh.elemental_fields['RHO']
    LAMBDA = get_lambda(VP, VS, RHO)
    MU     = get_mu(VS, RHO)
    mesh.attach_field('LAMBDA', LAMBDA)
    mesh.attach_field('MU', MU)
    
def compensate_density(mass_matrix: np.ndarray, density_matrix:np.ndarray, desired_mass:float, modifiable_part_mask:np.ndarray)->np.ndarray:
    """
    Changes the density matrix to sutisfy the desired mass for the cost of the volume defined by the mask.
    Return: new density mask
    """
    density_matrix = density_matrix.copy()
    initial_mass = np.sum(mass_matrix * density_matrix)
    mass_delta   = desired_mass - initial_mass
    
    modifiable_volume  = np.sum(mass_matrix[modifiable_part_mask])
    density_delta      = mass_delta / modifiable_volume
    print(f'Density change: {density_delta:.8}')
    
    density_matrix[modifiable_part_mask] += density_delta
    final_mass = np.sum(mass_matrix * density_matrix)
    np.testing.assert_allclose(final_mass, desired_mass,rtol = 1e-8)
    #assert final_mass == desired_mass, f'Diff: {final_mass - desired_mass}'
    return density_matrix