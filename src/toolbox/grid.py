import numpy as np
import pygmt
import xarray as xr

from salvus.flow import simple_config

def get_regular_station_grid(lat_step=2, lon_step=2, sidesetname = 'r1', fields = ['displacement'], **kwargs):
    """Returns a regular grid of SideSetPoint3D as an xarray"""
    
    for (key, val) in zip(['latmin', 'latmax', 'lonmin', 'lonmax'], [-90.+lat_step, 90., -180., 179.9]):
        if not key in kwargs: kwargs[key] = val

    latitudes  = np.arange(kwargs['latmin'], kwargs['latmax'], lat_step)
    longitudes = np.arange(kwargs['lonmin'], kwargs['lonmax'], lon_step)
    
    ss_points = np.empty((len(latitudes), len(longitudes)), dtype = simple_config.receiver.seismology.SideSetPoint3D)
    for i, lat in enumerate(latitudes):
        for j, lon in enumerate(longitudes):
            ss_points[i,j] = simple_config.receiver.seismology.SideSetPoint3D(
                            latitude      = lat, 
                            longitude     = lon, 
                            depth_in_m    = 0., 
                            station_code  = f'st{i}__{j}',
                            side_set_name = sidesetname, 
                            fields        = fields)
    return xr.Dataset  ( data_vars = {f'sideset_points_{fields[0]}':(['lat', 'lon'],ss_points)},
                         coords = {'lat': latitudes,
                                   'lon': longitudes})

    
def get_grid_DataArray(lat, lon, field):
    grid_lat = np.unique(lat)
    grid_lon = np.unique(lon)
    
    grid_dims  = (len(grid_lat), len(grid_lon))
    grid_field = field.reshape( grid_dims )
    
    grid_DataArray = xr.DataArray(data = grid_field,
                                  dims = [ 'lat', 'lon'],
                                  coords=dict(lat=grid_lat,
                                              lon=grid_lon))
    return grid_DataArray