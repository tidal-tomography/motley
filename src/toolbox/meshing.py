import numpy as np
from .constants import G

from salvus.mesh import simple_mesh

def create_mesh(paths: dict, parameters: dict):
    ms = simple_mesh.TidalLoading()
    #loading file path and which lmax is to be read
    ms.basic.tidal_loading_file      = str(paths['loading_file'].absolute())
    ms.basic.tidal_loading_lmax_1    = parameters['LMAX']
    ms.basic.local_refinement_level  = parameters['REFINEMENT_LEVEL']
    ms.basic.mantle_refinement_index    = 2
    #this number defines resolution of the mesh - divided by 3 it tells you how many elements are along an edge of the cube sphere
    ms.basic.nex                     = parameters['NEX']
    #1D model that's used to create the mesh
    ms.basic.model                   = parameters['ONED_MODEL']
    ms.basic.nelem_vertical          = parameters['NELEM_VERTICAL']
    ms.advanced.tensor_order         = parameters['TENSOR_ORDER']
    
    if parameters['TOPOGRAPHY']:
        ms.topography.topography_file    = paths['surface_topo']
        ms.topography.topography_varname = 'topography_earth2014_egm2008_lmax_256_lmax_256'
    if parameters['MOHO_TOPO']:
        ms.topography.moho_topography_file    = paths['moho_topo']
        ms.topography.moho_topography_varname = 'moho_topography_crust_1_0_egm2008_lmax_256'
    
    if parameters['GRAVITY']:
        ms.gravity_mesh.add_exterior_domain = True
        ms.gravity_mesh.nelem_buffer_outer  = parameters['NELEM_BUFFER_OUTER']
    
    mesh = ms.create_mesh(verbose=True)

    zeros = np.zeros((mesh.nelem, mesh.nodes_per_element))
    mesh.attach_field('zeros', zeros)
    fluid = np.zeros(mesh.nelem)
    mesh.attach_field('fluid', fluid)

    del mesh.elemental_fields['QKAPPA']
    del mesh.elemental_fields['QMU']

    return mesh

def prepare_gravity_mesh(input_mesh):
    '''
    add necessary fields and sidesets for the gravity simulation and return a copy of the mesh. 
    The input mesh stays untouched
    '''
    mesh = input_mesh.copy()
    f  = np.ones_like(mesh.elemental_fields['RHO'])
    mesh.elemental_fields['fluid'] = np.ones(mesh.nelem)
    M1 = 1 / (4 * np.pi * G)
    mesh.attach_field('M0' , f)
    mesh.attach_field('M1' , M1*f)
    
    for key in ['VP', 'VS',
                'GRAD_PHI_X', 'GRAD_PHI_Y', 'GRAD_PHI_Z', 'g',
                'zeros','external', 'tidal_elevation_amp', 
                'tidal_elevation_re', 'tidal_elevation_im', 
                'tidal_elevation_sea_mask_grad']: 
        if key in mesh.elemental_fields.keys(): del mesh.elemental_fields[key]
    return mesh