import numpy as np

PREM_MASS      = 5.9741640147753891e+24 #prem_iso_one_crust from salvus
EARTH_MASS     = 5.9733*10**24.
EARTH_MASS_ERR = 0.009 *10**24

INNER_CORE_R = 1220*10**3
CMB_R        = 3480*10**3  
EARTH_R      = 6371*10**3

MOI_MEAN_NORM     = 0.32997884
MOI_MEAN_NORM_ERR = 0.00000032

#[Marchenko&Schwitzer, 2003]
MOI_A = 0.32961433 
MOI_B = 0.32962160
MOI_C = 0.33070060
MOI_OBS   = np.sort([MOI_A, MOI_B, MOI_C])

G     = 6.67430e-11
M1    = 1 / (4 * np.pi * G)


def check_mass(mass:float)->float:
    """
    Input: mass to test
    Checks if the input within the observational error
    Output: the difference between the observed mean and the input
    """
    diff = np.abs(mass - EARTH_MASS)
    if diff > EARTH_MASS_ERR: 
        print( f'Mass constraint is not fit. Difference: {diff/10**24:.4}x10^24 kg' )
    else:                     
        print( f'Mass constraint is fit. Difference: {diff/10**24:>45}x10^24 kg' )
    return diff

def check_moi(moi:list)->np.array:
    diff = np.sort(moi) - MOI_OBS
    print(f'Observation: {MOI_OBS}')
    print(f'Model:       {moi}\n{"-"*40}')
    print(f'Difference:  {diff}')
    print(f'\nObservational error: {MOI_MEAN_NORM_ERR}')
    return diff

