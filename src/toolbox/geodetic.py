import numpy as np
#from mpl_toolkits.basemap import Basemap

def get_moi(
    node_masses,
    crd,
    nondiag=False,
    normalize=True,
):
    """
    Computes moment of inertia tensor
    node_masses: mass matrix multiplied by density nodal matrix
    crd:         coordinates of the nodes
    nondiag:     if True, non-diagonal elements are also computed
    normalize:   if True, the MoI values are divided by 1/(MR^2).
    """

    assert np.min(node_masses) > 0.0
    if normalize:
        BODY_R = np.max(np.linalg.norm(crd, axis=-1))
        BODY_MASS = np.sum(node_masses)
        MR2 = BODY_MASS * BODY_R**2

    MoI = np.zeros((3, 3))
    MoI[0, 0] = np.sum(node_masses * (crd[:, 1] ** 2 + crd[:, 2] ** 2))
    MoI[1, 1] = np.sum(node_masses * (crd[:, 0] ** 2 + crd[:, 2] ** 2))
    MoI[2, 2] = np.sum(node_masses * (crd[:, 0] ** 2 + crd[:, 1] ** 2))

    if nondiag:
        MoI[0, 1] = np.sum(node_masses * crd[:, 0] * crd[:, 1]) * (-1)
        MoI[0, 2] = np.sum(node_masses * crd[:, 0] * crd[:, 2]) * (-1)
        MoI[1, 2] = np.sum(node_masses * crd[:, 1] * crd[:, 2]) * (-1)
        MoI[1, 0] = MoI[0, 1]
        MoI[2, 0] = MoI[0, 2]
        MoI[2, 1] = MoI[1, 2]
        if normalize:
            return MoI / MR2
        else:
            return MoI
    else:
        if normalize:
            return MoI.diagonal() / MR2
        else:
            return MoI.diagonal()

def get_sph_coord(coords, degrees = False):
    """Returns spherical coordinates for the input array of Cartesian points. [latitude, longitude, radii]
    The value ranges for the coordinates: 
    latitude:  [-np.pi/2., np.pi/2.]
    longitude: (-np.pi, np.pi]"""
    longitudes = np.arctan2(coords[:,1], coords[:,0])
    latitudes  = np.arctan2(coords[:,2], np.linalg.norm(coords[:,0:2], axis=-1))
    radii      = np.linalg.norm(coords, axis=-1)
    sphcrd     = np.zeros_like(coords)
    if degrees: 
        sphcrd[:,0] = np.degrees(latitudes)
        sphcrd[:,1] = np.degrees(longitudes)
    else: 
        sphcrd[:,0] = latitudes
        sphcrd[:,1] = longitudes
    sphcrd[:,2] = radii
    return sphcrd

def get_mesh_mass(mesh):
    from .mass.elemental_matrices import get_mass_matrix
    
    assert 'RHO' in mesh.elemental_fields.keys()
    mass_matrix    = get_mass_matrix(mesh)
    density_matrix = mesh.elemental_fields['RHO']
    return get_mass(mass_matrix, density_matrix)

def test_mesh_vs_analytical(mass_matrix, crd):
    """compute mesh parameters that can be checked
    analytically for a `spherical` mesh
    input: mass matrix for the mesh
           radius of the mesh
    """
    # radius of the model sphere
    mesh_R = np.max(np.linalg.norm(crd, axis=-1))

    # volume
    mesh_V = np.sum(mass_matrix)
    analyt_V = 4 / 3.0 * np.pi * mesh_R**3
    diff_V = mesh_V - analyt_V
    print(f"V    Mesh: {mesh_V} Analyt: {analyt_V}")
    print(f"Rel diff : {(diff_V)/analyt_V}")

    # moi
    # assume RHO = 1  i.e. nodes masses = mass matrix -
    # does not matter what RHO factor to use for a homogeneous sphere
    mesh_moi = get_moi(mass_matrix, crd, nondiag=False, normalize=True)
    analyt_moi = 0.4
    diff_moi = mesh_moi - analyt_moi
    print(f"MoI  Mesh:{mesh_moi} Analyt: 0.4")
    print(f"Rel diff : {(diff_moi )/analyt_moi}")
    return [(mesh_V, diff_V), (mesh_moi, diff_moi)]

def convert_to_iso_longitudes(long):
    long = np.array(long)
    assert np.min(long) >= 0.; assert np.max(long) <= 360. 
    long[long > 180] -= 360
    return long

#def get_landmask(lats, lons):
#    """Returns landmask for the provided point coordinates
#    ---------
#    Input:
#    lat, lon - vectors containing latitudes and longitudes of the points
#    Output:
#    1D numpy.ndarray where 1 values correspond to the points lying within the continents
#    
#    Issues:
#    - For some reason, mpl_toolkits.basemap.Basemap does not mark Antarctica as a land.
#    - Not all Basemap projections contain landmask. 
#    """
#    assert len(lats) == len(lons), 'lats and lons vectors must have the same length'
#    N = len(lats)
#    bm = Basemap(projection='cyl',lon_0=0, resolution='c')
#    L = np.zeros(N, dtype=int)
#    for i, (lat, lon) in enumerate(zip(lats, lons)):
#        L[i] = bm.is_land(lon, lat)
#    return L
#
def get_landmask_reggrid(lat_grid, lon_grid):
    """Returns landmask for the regular grid
    --------------------------
    Input:
    lat_grid, lon_grid - vectors containing unique latitudes and longitudes of the grid
    Output:
    
    """
    lat_ravel = np.repeat(lat_grid, len(lon_grid))
    lon_ravel = np.tile  (lon_grid, len(lat_grid))
    landmask_ravel = get_landmask(lat_ravel, lon_ravel)
    return landmask_ravel.reshape((len(lat_grid), len(lon_grid)))
