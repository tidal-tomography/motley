# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import h5py
import numpy as np
import pygmt


from toolbox.grid_tools import get_grid_DataArray
from toolbox.plotting_tools import plot_DataArray
from toolbox.local_tools import read_receiver_solution
from toolbox.geodetic import get_sph_coord
# -

paths = {'point_re':'../output/full_alpha_05x05_one_crust_full/solution/point_solution_re.h5'}
paths = {'point_re':'../output/full_alpha_minimal_0.5x0.5/solution/point_solution_re.h5'}


def print_attrs(name, obj):
    print(name)
    for key, val in obj.attrs.iteritems():
        print(key, val)


dataset_re = h5py.File(paths['point_re'])
dataset_re.visititems(print)

# +
order = np.argsort(dataset_re['receiver_ids_ELASTIC_point'][:])

sph_crd = get_sph_coord(dataset_re['coordinates_ELASTIC_point'][:], degrees = True)
for i in range(2):
    sph_crd[:,i] = np.round(np.array(sph_crd[:,i]*10))/10
sph_crd[:,1][sph_crd[:,1] == -180] = 180


# -

landmask30m = pygmt.datasets.load_earth_mask(resolution="30m")
landmask_mesh = landmask30m.sel(lat = sv_da.lat,
                                lon = sv_da.lon)

fig = pygmt.Figure()
comp = ['x', 'y', 'z']
region = "-20/60/30/80"
with fig.subplot(nrows=1, ncols=3, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c", ):
    for p in range(3):
        da = get_grid_DataArray(lat   = sph_crd[:,0], 
                                lon   = sph_crd[:,1],
                                field = dataset_re['point/gradient-of-displacement'][:,p*4,-1][order])
        with fig.set_panel(panel=p):
            plot_DataArray(fig, 
                           da = da,
                           region=region,
                           title="eps_%s,%s" % (comp[p], comp[p]) )




fig.show(width=1500)
#fig.savefig('m2_strain_europe.pdf')


