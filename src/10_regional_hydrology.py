# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# %matplotlib inline
import os
import numpy as np
from pathlib import Path

import salvus.namespace as sn
from salvus.flow import api, simple_config

# +
PROJECT_DIR       = Path("../output/cone_load_to2")#Path("../output/zuri_see_load_to2")
topo_filename     = Path("../input/topo/gmrt_topography.nc")
mfile             = PROJECT_DIR / 'mesh.h5'
solution_filename = PROJECT_DIR / 'solution.h5'

parameters = {'MAX_ITERATIONS': 5000,
              'REL_TOLERANCE' : 1e-10,
              'SITENAME'      : 'local',
             'NRANKS'        : 126}


# -

def simulation(mfile, vout, parameters):
    """
        Run a simulation. 
            mfile    - mesh file path
            vout     - volumetric output file path
            pout     - pointwise output path
            stations - list of SideSetPoint3D objects
            parameters - simulation parameters
    """


    mesh = sn.UnstructuredMesh.from_h5(mfile)
    sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
    sim.domain.polynomial_order                      = mesh.shape_order

    sim.physics.elastostatic_equation.gravity                  = 'cowling'
    sim.physics.elastostatic_equation.right_hand_side.filename = str(mfile)
    sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"

    sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros", "zeros", "zeros"]

    sim.physics.elastostatic_equation.solution.filename        = vout.name
    sim.physics.elastostatic_equation.solution.fields          = ["solution",]# 'gradient-of-displacement']

    boundaries = simple_config.boundary.HomogeneousDirichlet(
        side_sets=["x0", "x1", 'y0', 'y1', 'z0']
    )
    sim.add_boundary_conditions(boundaries)

    boundaries = simple_config.boundary.Neumann(
        side_sets=["z1"]
    )
    sim.add_boundary_conditions(boundaries)

    sim.solver.max_iterations     = parameters['MAX_ITERATIONS']
    sim.solver.absolute_tolerance = 0
    sim.solver.relative_tolerance = parameters['REL_TOLERANCE']
    sim.solver.preconditioner     = True

    sim.validate()

    sn.api.run(
        input_file    = sim,
        site_name     = parameters['SITENAME'],
        output_folder = vout.parent,
        overwrite     = True,
        ranks         = parameters['NRANKS'],
        wall_time_in_seconds = 3600 # required only if you run the simulation on a cluster
    )

d = sn.domain.dim3.UtmDomain.from_spherical_chunk(
    min_latitude=46.5,
    max_latitude=47.5,
    min_longitude=8.0,
    max_longitude=9.0,
)
# Have a look at the domain to make sure it is correct.
d.plot()

# Now query data from the GMRT web service.
if not topo_filename.exists():
    d.download_topography_and_bathymetry(
        filename=topo_filename,
        # The buffer is useful for example when adding sponge layers
        # for absorbing boundaries so we recommend to always use it.
        buffer_in_degrees=0.1,
        resolution="med",
    )

# +
import netCDF4 as nc

# if src: src.close()
# if trg: trg.close()
src_file = topo_filename
trg_file = topo_filename.parent / 'cone.nc'

src = nc.Dataset(src_file)
trg = nc.Dataset(trg_file, mode='w')

center = {}
for key in ['lat', 'lon']:
    center[key] = (np.max(src.variables[key]) - np.min(src.variables[key]))/2. + np.min(src.variables[key])

R_EARTH = 6371e3

kmdeg = {}
kmdeg['lat'] = 40e6/360
kmdeg['lon'] = 2 * np.pi * R_EARTH * np.cos(np.radians(center['lat']))/360 

lon_mesh,lat_mesh = np.meshgrid(src.variables['lon'],
                                src.variables['lat'])

lat_m = (lat_mesh-center['lat'])*kmdeg['lat']
lon_m = (lon_mesh-center['lon'])*kmdeg['lon']

# +
R_cone = 20e3
H_cone = 10e3

cone       = np.zeros_like(src.variables['altitude'][:,:])
radii_mesh = (lat_m**2 + lon_m**2)**0.5
cone[radii_mesh < R_cone] = (R_cone - radii_mesh[radii_mesh < R_cone])/R_cone * H_cone

# +

# Create the dimensions of the file
for name, dim in src.dimensions.items():
    trg.createDimension(name, len(dim) if not dim.isunlimited() else None)
# Copy the global attributes
trg.setncatts({a:src.getncattr(a) for a in src.ncattrs()})

# Create the variables in the file
for name, var in src.variables.items():
    if name == 'altitude':
        trg.createVariable(name, var.dtype, var.dimensions)

        # Copy the variable attributes
        trg.variables[name].setncatts({a:var.getncattr(a) for a in var.ncattrs()})

        # Copy the variables values (as 'f4' eventually)
        trg.variables[name][:] = cone[:]
    else:
        trg.createVariable(name, var.dtype, var.dimensions)

        # Copy the variable attributes
        trg.variables[name].setncatts({a:var.getncattr(a) for a in var.ncattrs()})

        # Copy the variables values (as 'f4' eventually)
        trg.variables[name][:] = src.variables[name][:]
# -

# Load the data to Salvus compatible SurfaceTopography object. It will resample
# to a regular grid and convert to the UTM coordinates of the domain.
# This will later be added to the Salvus project.
t = sn.topography.cartesian.SurfaceTopography.from_gmrt_file(
    name="cone",
    data=topo_filename.parent / 'cone.nc',#topo_filename,
    resample_topo_nx=400,
    # If the coordinate conversion is very slow, consider decimating.
    decimate_topo_factor=5,
    # Smooth if necessary.
    gaussian_std_in_meters=0.0,
    # Make sure to pass the correct UTM zone.
    utm=d.utm,
)

t.ds.dem.T.plot(aspect=kmdeg['lat']/kmdeg['lon'], size=5)

# +
p = sn.Project.from_domain(path=PROJECT_DIR, domain=d, load_if_exists=True)
p += t
bm = sn.model.background.homogeneous.IsotropicElastic(
    rho=2600.0, vp=3000.0, vs=1875.5
)
mc = sn.model.ModelConfiguration(background_model=bm)
tc = sn.topography.TopographyConfiguration(topography_models="cone")#"zuri_see")

p += sn.SimulationConfiguration(
    name="cone_topo",#"zuri_see_topo",
    tensor_order=2,
    model_configuration=mc,
    event_configuration=None,
    elements_per_wavelength=2,
    max_depth_in_meters=50000.0,
    max_frequency_in_hertz=1.0,
    topography_configuration=tc,
)

# +
mesh = p.simulations.get_mesh('cone_topo')#zuri_see_topo')
mesh_shape = (mesh.nelem, mesh.nodes_per_element)
mesh.attach_field('KAPPA',      1e9*np.ones(mesh_shape))
mesh.attach_field('MU'   ,      1e9*np.ones(mesh_shape))
mesh.attach_field('GRAD_PHI_Z', 9.8*np.ones(mesh_shape))
mesh.attach_field('GRAD_PHI_X', np.zeros(mesh_shape))
mesh.attach_field('GRAD_PHI_Y', np.zeros(mesh_shape))
mesh.attach_field('zeros',      np.zeros(mesh_shape))

middle_point = np.array([np.mean(mesh.points[:,0]), np.mean(mesh.points[:,1])])
print('middle point coordinates:', middle_point)
print('max radius:', np.max(np.linalg.norm(mesh.points[:,:2]  - middle_point, axis=-1)))
NEUMANN = np.zeros(mesh_shape)
DISK_RADIUS = 10000
#shift the load by a distance equal to the disk load radius
middle_point[1] += DISK_RADIUS
mask    = (np.linalg.norm(mesh.points[:,:2]  - middle_point, axis=-1) < DISK_RADIUS)[mesh.connectivity]
NEUMANN[mask] = -100
mesh.attach_field('NEUMANN', NEUMANN)

for key in ['VP', 'VS']:
    if key in mesh.elemental_fields.keys():
        del mesh.elemental_fields[key]
# -

mesh.write_h5(mfile)

simulation(mfile=mfile, vout=solution_filename, parameters=parameters)


