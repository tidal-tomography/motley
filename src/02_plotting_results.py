# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

#it's imporant to import netcdf before pygmt
from netCDF4 import Dataset

# +
import pygmt
import numpy as np
import xarray as xr

# from mpl_toolkits.basemap import Basemap
# -
PROJECTION    = "R12c"
LOADDATA_FILE = "../input/christian_results/load-m2-tide-imag-homogeneous.nc"
DISPLACEMENT_FILE = "../input/christian_results/displacement-m2-tide-imag-homogeneous.nc"

M2_DISPL_FILE = '../output/full_alpha_05x05_one_crust_full/pics/station_grid_notopo.nc'

loaddata  = xr.open_dataarray(LOADDATA_FILE)
displdata = xr.open_dataarray(DISPLACEMENT_FILE)
m2displdata = xr.open_dataset(M2_DISPL_FILE)

lats = loaddata.latitude.data
lons = loaddata.longitude.data


def get_landmask(lats, lons):
    """Returns landmask for the provided point coordinates
    ---------
    Input:
    lat, lon - vectors containing latitudes and longitudes of the pointsw
    Output:
    1D numpy.ndarray where 1 values correspond to the points lying within the continents
    
    Issues:
    - For some reason, mpl_toolkits.basemap.Basemap does not mark Antarctica as a land.
    - Not all Basemap projections contain landmask. 
    """
    assert len(lats) == len(lons), 'lats and lons vectors must have the same length'
    N = len(lats)
    bm = Basemap(projection='cyl',lon_0=0, resolution='c')
    L = np.zeros(N, dtype=int)
    for i, (lat, lon) in enumerate(zip(lats, lons)):
        L[i] = bm.is_land(lon, lat)
    return L


# +
def get_landmask_reggrid(lat_grid, lon_grid):
    """Returns landmask for the regular grid
    --------------------------
    Input:
    lat_grid, lon_grid - vectors containing unique latitudes and longitudes of the grid
    Output:
    
    """
    lat_ravel = np.repeat(lat_grid, len(lon_grid))
    lon_ravel = np.tile  (lon_grid, len(lat_grid))
    landmask_ravel = get_landmask(lat_ravel, lon_ravel)
    return landmask_ravel.reshape((len(lat_grid), len(lon_grid)))

def plot_DataArray(fig, da, cb_name='', cb_units = 'm', title="", projection = "R12c", region='d', series=[], landmask = False, cmap='polar', reverse_cmap=False):
    """
    Adds figure based on data from dataarray toa a pygmt.Figure object
    ---------
    Input:
    da       - xarray.DataArray to be plotted
    cb_name  - caption under the colorbar 
    cb_units - caption at the colorbar. Usually used for marking units (default: m).
    """
    if len(series) > 0:
        pygmt.makecpt( series=series, cmap=cmap, reverse=reverse_cmap)
    fig.grdimage(da, region=region, frame=["afg", f'+t"{title}"'], projection=projection)
    
    coastparams = dict(shorelines="0.5p,darkgray", region=region, frame=["a", f'+t""'], projection=projection)
    if landmask: coastparams['land'] = '#FFFFFF'
    fig.coast(**coastparams)
     
    if len(cb_name)>0: xcb = f'x+l"{cb_name}"'
    else: xcb = ""
    fig.colorbar(frame=[ xcb, f'y+l"{cb_units}"'], projection=projection,) 
    return fig


# +
# lm = get_landmask_reggrid(lats.data, lons.data)

# +
# landmaskda = xr.DataArray(  data   = lm.reshape((len(lats.data), len(lons.data))),
#                             dims   = ['latitude', 'longitude'],
#                             coords = dict(latitude=lats.data,
#                                           longitude=lons.data))
# -

landmask = pygmt.datasets.load_earth_mask(resolution="30m")

mask = np.array(
landmask.sel(lat = np.array(lats/0.5, dtype=int) * 0.5, 
             lon = np.array(lons/0.5, dtype=int) * 0.5,)
,dtype=bool)
oceanmask = np.array(mask-1,dtype=bool)

mask = np.array(mask, dtype = float)
mask[mask==0] = np.nan
oceanmask = np.array(oceanmask, dtype = float)
oceanmask[oceanmask == 0] = np.nan

fig = pygmt.Figure()
with fig.subplot(nrows=1, ncols=2, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c"):
    for p in range(2):
            if p == 0: 
                da       = loaddata.sel(depth=0)*oceanmask
                cb_name  = 'M2 ocean tide water level'
                cb_units = 'm'
                series   = [-2.5, 2.5]
                land     = False
                cmap     = 'polar'
                reverse_cmap = False
            if p == 1: 
                sc_fact  = 100
                da       = displdata.sel(depth=0)*sc_fact*mask
                cb_name  = 'Surface displacement amplitude'
                cb_units = 'cm'
                land     = False
                series  = [0., displdata.data.max()*sc_fact]
                cmap    = 'hot'
                reverse_cmap = True
            with fig.set_panel(panel=p):
                plot_DataArray(fig, da, cb_name=cb_name, 
                               region='d', cb_units=cb_units, 
                               landmask= land, cmap = cmap, reverse_cmap=reverse_cmap,
                               series = series, projection = PROJECTION,)

fig
#fig.savefig('plot9.pdf')

input_ds = netCDF4.Dataset('../input/load_models/tidal_load_m2_10800_new_grad_new_mask.nc')

LMAX = 512
load_da = xr.DataArray(dims   = ['lat', 'lon'],
                       coords = dict(lat = input_ds[f"latitude_tidal_elevation_re_lmax_{LMAX}"][:],
                                     lon = input_ds[f"longitude_tidal_elevation_re_lmax_{LMAX}"][:]),
                       data   = input_ds[f"tidal_elevation_amp_lmax_{LMAX}"][:].data.T)

m2displdata = m2displdata.assign(displacement_amp = (m2displdata.displacement_re**2 + m2displdata.displacement_im**2)**0.5 )

# +
fig = pygmt.Figure()
projection = "R12c"
region='d'
frame_gmt_code = ["afg"]
labels = {'load' :'Ocean tide amplitude',
          'displ':'Vertical displacement amplitude'}
pygmt.config(FONT_LABEL='10p,Helvetica,black')


da_load    = load_da#m2displdata.displacement_amp.sel(VNE='V')*oceanmask
series= [0., 2.]#int(da_load.max().data)]
pygmt.makecpt(series=series, cmap='seafloor',reverse=bool(reverse_cmap))

fig.grdimage(da_load, region=region, frame=frame_gmt_code, projection=projection, nan_transparent = True)
fig.colorbar(frame=[ f"x+l{labels['load']}", f'y+l"m"'],
             position="JCR+w6c/0.25c+v", projection=projection,) 

sc_fact = 1000
mask   = np.array(landmask.sel(lat=m2displdata.lat, lon = m2displdata.lon).data, dtype=float)
mask[mask == 0] = np.nan
da_displ     = m2displdata.displacement_amp.sel(VNE='V')*mask*sc_fact
series = [0., 40]#int(da.max())]
pygmt.makecpt(series=series, cmap='hot', reverse=bool(reverse_cmap))
fig.grdimage(da_displ,  region=region, frame=frame_gmt_code, projection=projection, nan_transparent = True)

coastparams = dict(shorelines=f"0.5p", region=region,  projection=projection)
fig.coast(**coastparams)
fig.colorbar(frame=[ f"x+l{labels['displ']}", f'y+l"mm"'],
             position="JBC+w10c/0.25c+h", projection=projection,) 
fig.show()
# -
fig.savefig('m2load+displ_prem.pdf')


