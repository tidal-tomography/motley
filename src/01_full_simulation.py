# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

# +
# gradint of phi - compare to the volumetric output
# cowling        - 3 components and what are the results
# full           - 3 components and what are the amplitudes
# meta.json file

# +
import numpy as np
import os
os.environ['GMT_LIBRARY_PATH']='/home/andreid/mambaforge/envs/pygmt/lib'
import pygmt
import xarray as xr

from toolbox.stdout import Stdout
from toolbox.local_tools   import create_mesh, \
                                  prepare_gravity_mesh, gravity_simulation,  read_solution, simulation,\
                                  read_receiver_solution, \
                                  plot_all_components
from toolbox.meshing_tools import tell_me_about_mesh, get_layer_mask, get_element_rotation_matrix
from toolbox.containers    import Paths, Parameters
from toolbox.constants     import EARTH_R, EARTH_MASS, PREM_MASS
from toolbox.general_tools import get_land_mask
from toolbox.grid_tools    import add_SideSetPoint3D, get_regular_station_grid,\
                                    get_grid_DataArray, plot_DataArray
from toolbox.paramet_tools import compensate_density
from toolbox.mass.elemental_matrices import get_mass_matrix
from toolbox.geodetic      import convert_to_iso_longitudes, get_sph_coord, get_landmask_reggrid

import salvus.namespace as sn

complex_components = ['re', 'im']
displ_components   = ['V', 'N', 'E']

# +
lat_step = 2
lon_step = 2

station_grid1 = get_regular_station_grid(lat_step = lat_step, lon_step=lon_step, fields = ['phi', 'gradient-of-phi'],latmin=-89,latmax=90, lonmin=-180, lonmax=179)
station_grid2 = get_regular_station_grid(lat_step = lat_step, lon_step=lon_step, fields = ['displacement', 'gradient-of-displacement'], latmin=-89,latmax=90, lonmin=-180, lonmax=179)
station_grid  = station_grid1.combine_first(station_grid2)

# +
parameters = Parameters()
#paths['loading_model']= '../input/load_models/got410c.m2.dat'
parameters['GRAVITY']          = True
parameters['TENSOR_ORDER']     = 4
parameters['NEX']              = 24
parameters['NELEM_VERTICAL']   = [2,2]
parameters['REL_TOLERANCE']    = 1e-10
parameters['REFINEMENT_LEVEL'] = 1
parameters['ONED_MODEL']       = 'prem_iso_one_crust'#'../input/1d_models/kemper23_smks2.bm'

parameters['SITENAME'] = 'local'
parameters['NRANKS']   = 100

paths = Paths(f"full_alpha_to-{parameters['TENSOR_ORDER']}_reflev-{parameters['REFINEMENT_LEVEL']}_grid-{lat_step}x{lon_step}")
paths.setup_folder()

run_gravity                    = True #False
show_figures                   = False

print(parameters, paths)

# +
mesh = create_mesh(paths, parameters)

# mass_matrix    = get_mass_matrix(mesh)
# density_matrix = mesh.elemental_fields['RHO']
# desired_mass   = PREM_MASS
# layer_mask     = get_layer_mask(mesh, nlayer=[8,9])

# updated_density_matrix = compensate_density(mass_matrix, density_matrix, desired_mass, layer_mask)
# mesh.elemental_fields['RHO'] = updated_density_matrix

# +
if parameters['GRAVITY']:
    gravity_mesh = prepare_gravity_mesh(mesh)    
    gravity_mesh.write_h5(paths.gravity_mesh_file)
    if run_gravity:
        gravity_simulation(mfile    = paths.gravity_mesh_file, 
                         vout       = paths.gravity_solution,
                         parameters = parameters,
                         stations   = station_grid['sideset_points_phi'].data.ravel(),
                         pout       = paths.gravity_psolution)
        std = Stdout(os.path.dirname(paths.gravity_solution) + '/stdout' )
        print(f'Relative error at the last iteration (#{len(std.data[:,2])}): {std.data[-1,2]}')
    del gravity_mesh

gravity_solution = read_solution(paths.gravity_solution, 'gradient')
for comp in ['X', 'Y', 'Z']:
    mesh.elemental_fields[f'GRAD_PHI_{comp}'] = gravity_solution[f'gradient-of-phi_{comp}']
g = np.linalg.norm([mesh.elemental_fields[f'GRAD_PHI_{comp}'] for comp in ['X', 'Y', 'Z']], axis=0)
mesh.attach_field('g', g)

elastos_mesh = mesh.copy()

tell_me_about_mesh(elastos_mesh);

# +
gradphi = read_receiver_solution(paths.gravity_psolution, field= 'gradient-of-phi')
phi     = read_receiver_solution(paths.gravity_psolution, field= 'phi')

station_grid = station_grid.assign(gradphi = (['lat', 'lon', 'VNE'], gradphi[0].reshape(station_grid.sideset_points_phi.shape + (3,))))
station_grid = station_grid.assign(    phi = (['lat', 'lon'],            phi[0].reshape(station_grid.sideset_points_phi.shape)))
# -

fig = pygmt.Figure()
with fig.subplot(nrows=2, ncols=2, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c", ):
    with fig.set_panel(panel=0):
        plot_DataArray(fig, da = station_grid['phi'])
    for p in range(1,4):
        with fig.set_panel(panel=p):
            plot_DataArray(fig, da = station_grid['gradphi'][:,:,p-1])

if show_figures: fig.show()
fig.savefig(paths.pictures / 'gravity.png')

# +
# elastos_mesh = mesh.apply_element_mask(np.array(mesh.elemental_fields['external']-1, dtype=bool))
# parameters['GRAVITY'] = False
# elastos_mesh.write_h5(paths.elastos_mesh_file)

elastos_mesh = mesh
elastos_mesh.attach_field('DN_RHO_G', np.zeros((mesh.nelem, mesh.nodes_per_element)))

# +
parameters['SITENAME']       = 'tides_alpha'
parameters['MAX_ITERATIONS'] = 40000

for cpl_comp in complex_components:
    #adding Neumann BC to the mesh - it an elemental_field which is non-zero only at the surface. 'tidal_elevation_re' field was added to the mesh by Martin's meshing routine:
    M2_load = (-1) * elastos_mesh.element_nodal_fields[f'tidal_elevation_{cpl_comp}'] * 1030. #* elastos_mesh.elemental_fields['g'] 
    elastos_mesh.attach_field('NEUMANN', M2_load)
    elastos_mesh.write_h5(paths.elastos_mesh_file)
    
    _ = simulation(stations  = station_grid.sideset_points_displacement.data.ravel(), 
                   mfile     = str(paths['elastos_mesh_file']), 
                   vout      = str(paths[f'vol_solution_{cpl_comp}']), 
                   pout      = str(paths[f'point_solution_{cpl_comp}']),
                   parameters= parameters,
                   cowling_or_full='full')
    std = Stdout(os.path.dirname(paths.gravity_solution) + '/stdout' )
    if show_figures: std.plot(2)
    print(f'Relative error at the last iteration (#{len(std.data[:,2])}): {std.data[-1,2]}')
# -
for cpl_comp in complex_components:
    point_solution, _ = read_receiver_solution(filename = paths[f'point_solution_{cpl_comp}'],
                                                                    field = 'displacement',
                                                                    acoustic_or_elastic='ELASTIC')
    station_grid = station_grid.assign_coords(coords={'VNE': ['V','N','E']})
    shape = station_grid.lat.shape + station_grid.lon.shape + (3,)
    station_grid = station_grid.assign({f'displacement_{cpl_comp}': (['lat','lon','VNE'], point_solution.reshape(shape))})
#displ_v_mag = np.linalg.norm([station_grid.displacement_re.sel(VNE='V'), station_grid.displacement_im.sel(VNE='V')], axis=0)
#station_grid = station_grid.assign(displacement_V_magnitude=(['lat','lon'],displ_v_mag))
station_grid = station_grid.assign(displacement_re_magnitude=(['lat','lon'],\
                                                              np.linalg.norm(station_grid.displacement_re,axis=-1) ))

for key in ['sideset_points_displacement','sideset_points_phi']:
    if key in station_grid.keys():
        del station_grid[key]
station_grid.to_netcdf(paths.pictures / 'station_grid_notopo.nc')

# +
# landmask = get_landmask_reggrid( station_grid.lat.data,
#                                  station_grid.lon.data,
#                                  )
# -

fig = pygmt.Figure()
with fig.subplot(nrows=2, ncols=2, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c", ):
    with fig.set_panel(panel=0):
        plot_DataArray(fig, 
                       da = station_grid['displacement_re_magnitude'], #* landmask,
                       cb_name = 'displacement_re_magnitude',
                       cb_units = 'm')
    for p in range(1,4):
        with fig.set_panel(panel=p):
            component = list(['V','N','E'])[p-1]
            plot_DataArray(fig, 
                           da = station_grid['displacement_re'][:,:,p-1],#* landmask,
                           cb_name = f'displacement_re_{component}',
                           cb_units = 'm')

if show_figures: fig.show()

#fig.show()
fig.savefig(paths.pictures / 'displacement_re.png')



paths.pictures / 'station_grid_notopo.nc'

#loaddef simulation results:
ld_data = np.loadtxt('../input/stations/cn_OceanOnly_GOT410c-M2_cm_convgf_GOT410c_global_PREM.txt', skiprows=1)

# +
from toolbox.geodetic import convert_to_iso_longitudes
lat     = np.unique(ld_data[:,1])
lon     = convert_to_iso_longitudes(np.unique(ld_data[:,2]))
shape   = (len(lat), len(lon)) 
displ_v = ld_data[:,7]

lon_order = np.argsort(lon)
ld_da   = xr.DataArray(data = displ_v.reshape(shape)[:,lon_order],
             dims = ['lat', 'lon'],
             coords=dict(lat = lat, 
                         lon = lon[lon_order]))
sv_da = ((station_grid.displacement_re**2 + station_grid.displacement_im**2)**0.5*1000)[:-1,:,:].sel(VNE='V')
# -

fig = pygmt.Figure()
with fig.subplot(nrows=1, ncols=3, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c", ):
    for p in range(2):
        with fig.set_panel(panel=p):
            da = [ld_da, sv_da, ld_da - sv_da]
            labels = ['LoadDef', 'Salvus', 'Difference']
            plot_DataArray(fig, 
                           da = da[p],#* landmask,
                           series = [0,55],
                           cb_name = labels[p],
                           cb_units = 'mm')
    p = 2
    with fig.set_panel(panel=p):
        plot_DataArray(fig, 
                       da = da[p],
                       cb_name = labels[p],
                       series  = [-10,10],
                       cb_units = 'mm')
fig.show()
fig.savefig('Salvus-LoadDef_comparison_full_to4.pdf')




