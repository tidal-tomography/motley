# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.15.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import numpy as np
import netCDF4
import pygmt
import xarray as xr
from scipy.interpolate import RectSphereBivariateSpline

from toolbox.grid_tools import plot_DataArray
from toolbox.geodetic   import convert_to_iso_longitudes

# +
input_ds = netCDF4.Dataset('../input/load_models/got4.10m2.nc')
amp   = input_ds.variables['amplitude']
phase = input_ds.variables['phase']


re    = amp*np.cos(np.radians(phase))
im    = amp*np.sin(np.radians(phase))

# all zero values are replaces with very unrealisticly big numbers in the netcdf file. 
# Put zeros back in place
for arr in [re, im]:
    arr[np.abs(arr)>1e6] = 0

# +
# The longitudes are in [0,360] format. Convert it to the iso one.
lon = convert_to_iso_longitudes(np.array(input_ds.variables['longitude']))
order = np.argsort(lon)

ds = xr.Dataset(data_vars = dict(
                        re = (('lat', 'lon'), re[:,order]),
                        im = (('lat', 'lon'), im[:,order])
                            ),
           coords = dict(
                        lat = ('lat', np.array(input_ds.variables['latitude'])),
                        lon = ('lon', lon[order]),
                        ),
           attrs  = dict(units='cm',))
# -

fig = pygmt.Figure()
with fig.subplot(nrows=1, ncols=2, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c", ):
    for p, name in enumerate(list(ds.keys())):
        with fig.set_panel(panel=p):
            plot_DataArray(fig, 
                           ds[name],
                           series  = [-150,150],
                           cb_units= ds.attrs['units'],
                           cb_name = name
                           )
fig.show()

ds.to_netcdf('../input/load_models/got410m2_reim.nc')

# +
loadraw = xr.open_dataset('../input/load_models/got410m2_reim.nc')

tol = 1e-6
lat = np.radians(loadraw['real'].lat + 90 + tol)
lat[-1] -= tol
spline = RectSphereBivariateSpline(u = lat,
                          v = np.radians(loadraw['real'].lon),
                          r = loadraw['real'].data)

# +
theta, phi = np.meshgrid( np.arange(-0, 180, 1), np.arange(-180, 180, 1))

m2_spline = spline.ev(theta = np.radians(theta.ravel()), 
                      phi   = np.radians(phi.ravel()))
# -

fig = pygmt.Figure()
plot_DataArray(fig, 
               xr.DataArray(dims=['lat', 'lon'],
                            coords=dict(lat = theta[0]-90,
                                        lon = phi[:,0]),
                            data = m2_spline.reshape(theta.shape).T),
              series = [-100,100])

np.radians(phi.ravel())


