# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# + deletable=false
# %autoreload 2

# + deletable=false slideshow={"slide_type": "slide"}
import numpy as np
import os
from pathlib import Path
from toolbox.grid          import get_regular_station_grid
from toolbox.containers    import Paths, Parameters
from toolbox.constants     import EARTH_R, CMB_R, INNER_CORE_R, EARTH_MASS, G
from toolbox.meshing       import create_mesh, prepare_gravity_mesh
from toolbox.meshing_tools import find_sideset, tell_me_about_mesh
from toolbox.simulation    import gravity_config, elastic_config, run_simulation
from toolbox.solution      import read_solution, read_receiver_solution

complex_components = ['re', 'im']
displ_components   = ['V', 'N', 'E']

# + deletable=false
lat_step = 10
lon_step = 10

station_grid = get_regular_station_grid(lat_step = lat_step, lon_step=lon_step, fields = ['displacement', 'gradient-of-displacement'], 
                                        latmin=-89,latmax=90, lonmin=-180, lonmax=179)

# + deletable=false
parameters = Parameters()
paths = Paths(f'full_alpha_minimal_{lat_step}x{lon_step}_incore')
paths['loading_file'] = Path(os.environ.get('LOADING_FILE', paths['loading_file']))
paths.setup_folder()

parameters['GRAVITY']          = True
parameters['TENSOR_ORDER']     = 1
parameters['NEX']              = 18
parameters['NELEM_VERTICAL']   = [2,2]
parameters['REL_TOLERANCE']    = 1e-10
parameters['REFINEMENT_LEVEL'] = 1
parameters['ONED_MODEL']       = 'prem_iso_one_crust'#'../input/1d_models/kemRper23_smks2.bm'
print(parameters, paths)

parameters['SITENAME'] = 'local'
parameters['NRANKS']   = 10

# + deletable=false
mesh = create_mesh(paths, parameters)
find_sideset(mesh, CMB_R, tolerance = 10e3)


# + deletable=false
gravity_mesh = prepare_gravity_mesh(mesh);
gravity_mesh.write_h5(paths.gravity_mesh_file)

gravity_sim  = gravity_config(mfile         = paths.gravity_mesh_file, 
                              vout          = paths.gravity_solution,
                              polynom_order = parameters['TENSOR_ORDER'],
                              oned_model    = parameters['ONED_MODEL'],)

print(gravity_sim.get_dictionary()['physics'])

run_simulation(   sim       = gravity_sim,
                  directory = paths.gravity_solution.parent,
                  parameters= parameters)

# +
gravity_solution = read_solution(paths.gravity_solution, 'gradient')

for comp in ['X', 'Y', 'Z']:
    mesh.elemental_fields[f'GRAD_PHI_{comp}'] = gravity_solution[f'gradient-of-phi_{comp}']
g = np.linalg.norm([mesh.elemental_fields[f'GRAD_PHI_{comp}'] for comp in ['X', 'Y', 'Z']], axis=0)

mesh.attach_field('g', g)

# + deletable=false
elastos_mesh = mesh
elastos_mesh.elemental_fields['fluid'] *= 0
elastos_mesh.attach_field('DN_RHO_G', np.zeros((mesh.nelem, mesh.nodes_per_element)))

# +
parameters['SITENAME'] = 'tides_alpha'
parameters['REL_TOLERANCE'] = 0.005

for cpl_comp in complex_components:
    #adding Neumann BC to the mesh - it an elemental_field which is non-zero only at the surface. 'tidal_elevation_re' field was added to the mesh by Martin's meshing routine:
    M2_load = (-1) * elastos_mesh.element_nodal_fields[f'tidal_elevation_{cpl_comp}'] * 1030. #* elastos_mesh.elemental_fields['g'] 
    elastos_mesh.attach_field('NEUMANN', M2_load)
    elastos_mesh.write_h5(paths.elastos_mesh_file) 
    
    elastos_sim = elastic_config( mfile           = str(paths['elastos_mesh_file']), 
                                  vout            = str(paths[f'vol_solution_{cpl_comp}']), 
                                  polynom_order   = parameters['TENSOR_ORDER'],  
                                  cowling_or_full ='full',
                                )
    
    print(elastos_sim.get_dictionary()['physics'])
    
    _ = run_simulation(   sim       = elastos_sim,
                          directory = str(paths[f'vol_solution_{cpl_comp}'].parent),
                          parameters= parameters,
                          stations  = station_grid.sideset_points_displacement.data.ravel(),
                          pout      = str(paths[f'point_solution_{cpl_comp}'].name)
                      )
# +
for cpl_comp in complex_components:
    point_solution, _ = read_receiver_solution(filename = paths[f'point_solution_{cpl_comp}'],
                                                                    field = 'displacement',
                                                                    acoustic_or_elastic='ELASTIC')
    station_grid = station_grid.assign_coords(coords={'VNE': ['V','N','E']})
    shape = station_grid.lat.shape + station_grid.lon.shape + (3,)
    station_grid = station_grid.assign({f'displacement_{cpl_comp}': (['lat','lon','VNE'], point_solution.reshape(shape))})

station_grid = station_grid.assign(displacement_re_magnitude=(['lat','lon'],\
                                                              np.linalg.norm(station_grid.displacement_re,axis=-1) ))
# -

if 'sideset_points_displacement' in station_grid:
    del station_grid['sideset_points_displacement']
station_grid.to_netcdf(str(paths.point_solution_table))
print(f'station grid was saved to {paths.point_solution_table}')
