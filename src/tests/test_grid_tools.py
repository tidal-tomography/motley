# +
import numpy as np

from .grid_tools import get_regular_station_grid
from .geodetic   import get_sph_coord


# -

def test_get_regular_station_grid():
    """Compute coordinates of the SideSet3D and compare it to the expected coordinates"""
    grid = get_regular_station_grid()
    sph_points = [] #Coordinates
    crt_points = [] #Cartesian
    for i in range(0, len(grid.lat), 5):
        for j in range(0, len(grid.lon), 5):
            crt_points.append(list(grid[i,j].item(0).point))
            sph_points.append([float(grid.lat[i]), float(grid.lon[j])])

    np.testing.assert_allclose(get_sph_coord(np.array(crt_points), degrees=True)[:, :2], sph_points,rtol=1e-12)
