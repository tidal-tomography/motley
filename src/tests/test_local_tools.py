from salvus.mesh import simple_mesh
from salvus.flow import simple_config
import salvus.namespace as sn

import numpy as np
import random
import os
import shutil

def check_and_rmtree(directory):
    abspath = os.path.abspath(directory)
    assert len(abspath.split('/')) > 6
    assert abspath not in ['/', '/Users']
    assert os.path.abspath(directory).split('/')[-2] == 'src'
    assert '.test' in os.path.basename(abspath)
    shutil.rmtree(directory)

def test_simple_gravity():
    testpath     = f'./.test_{int(random.random()*1e8)}/'
    meshpath     = testpath + 'mesh.h5'
    solutionpath = testpath + 'gravity_solution.h5'
    
    os.makedirs(os.path.dirname(testpath))
    
    ms = simple_mesh.Globe3D()
    ms.basic.model                   = 'prem_iso_one_crust'
    ms.basic.elements_per_wavelength = 1.
    ms.basic.min_period_in_seconds   = 200.
    ms.gravity_mesh.add_exterior_domain = True
    mesh = ms.create_mesh()
    
    ones = np.ones((mesh.nelem, mesh.nodes_per_element))
    mesh.attach_field('fluid', np.ones(mesh.nelem))
    mesh.attach_field('M0', ones)
    mesh.attach_field('M1', ones)
    mesh.attach_field('RHS', mesh.elemental_fields['RHO'])
    for key in ['VP', 'VS']: del mesh.elemental_fields[key]
    mesh.write_h5(meshpath)
    
    sim                                                   = sn.simple_config.simulation.Poisson(mesh=mesh)
    sim.domain.polynomial_order                           = mesh.shape_order
    sim.physics.poisson_equation.right_hand_side.filename = meshpath
    sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
    sim.physics.poisson_equation.right_hand_side.field    = "RHS"
    sim.physics.poisson_equation.solution.filename        = os.path.basename(solutionpath)
    sim.physics.poisson_equation.solution.fields          = ['solution',"gradient-of-phi"]

    boundaries               = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"])
    sim.add_boundary_conditions(boundaries)

    sim.solver.max_iterations     = 1
    sim.solver.absolute_tolerance = 0.0
    sim.solver.relative_tolerance = 1e-10
    sim.solver.preconditioner     = True

    sim.validate()

    sn.api.run(
        input_file    = sim,
        site_name     = 'local',
        output_folder = os.path.dirname(solutionpath),
        ranks         = 1,
        overwrite     = True
    )
    check_and_rmtree(testpath)
        
    print('--------- test_simple_gravity() has succeded ---------')