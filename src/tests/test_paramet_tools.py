# %load_ext autoreload

# %autoreload 2

# +
import numpy as np
import pytest

from paramet_tools import compensate_density
from salvus.mesh import simple_mesh


# -

def test_compensate_density():

    boundaries = np.array([1000, 2000, 4000])*10**3
    with open('radial_model.bm', 'w') as file: 
        file.write(
f"""NAME         radial_model
UNITS        m
COLUMNS      RADIUS RHO VP VS
    0.0    1000.0     5000.0    5000.0
 {boundaries[0]}\t1000.0\t5000.0\t5000.0
 {boundaries[0]}\t1000.0\t5000.0\t5000.0
 {boundaries[1]}\t1000.0\t5000.0\t5000.0
 {boundaries[1]}\t7710.0\t5000.0\t5000.0
 {boundaries[2]}\t7710.0\t5000.0\t5000.0
 {boundaries[2]}\t10000.0\t5000.0\t5000.0
 6371e3   10000.0    5000.0    5000.0"""
        )
    
    ms = simple_mesh.Globe3D()
    ms.basic.model                   = 'radial_model.bm'
    ms.basic.elements_per_wavelength = 1.
    ms.basic.min_period_in_seconds   = 400.
    ms.advanced.tensor_order         = 2
    ms.gravity_mesh.add_exterior_domain = True

    mesh = ms.create_mesh()
    return mesh

mesh = test_compensate_density()

# !open ../..

mesh.write_h5('../../output/trash/mesh.h5')

mesh.nelem

mesh.nodes_per_element


