from .geodetic import get_moi, get_sph_coord
import numpy as np
import pytest
from .mass.elemental_matrices import get_mass_matrix
from salvus.mesh.simple_mesh import Globe3D

R = 6.283
@pytest.mark.parametrize(
    "point_coords, degrees, expected_crd",
    [
        (
            np.array([[R, 0, 0],
                      [0, R, 0],
                      [0, 0, R],
                      [-R, 0, 0],
                      [0, -R, 0],
                      [0, 0, -R],
                      [R, 0,  R],
                      [R, R,  0],
                      [R, 0, -R],
                      [R, -R, -R]]),
            False, 
            np.array( [[0,          0, R],
                       [0,    np.pi/2, R],
                       [np.pi/2,    0, R],
                       [0,      np.pi, R],
                       [0,  -np.pi/2., R],
                       [-np.pi/2.,  0, R],
                       [ np.pi/4., 0, np.sqrt(2*R**2)],
                       [0,  np.pi/4., np.sqrt(2*R**2)],
                       [-np.pi/4., 0, np.sqrt(2*R**2)],
                       [-np.arctan(R/np.sqrt(2*R**2)), -np.pi/4., np.sqrt(3*R**2)]]),
        )
    ],
)
def test_get_sph_coord(point_coords, degrees, expected_crd):
    print(get_sph_coord(point_coords, degrees) - expected_crd)
    np.testing.assert_allclose(get_sph_coord(point_coords, degrees),
                               expected_crd, rtol = 1e-8)


@pytest.mark.parametrize(
    "point_masses, point_coords, expected_moi",
    [
        (
            np.array([1.7426, 10.2341, 100.238952]),
            np.array(
                [[2.5, 3.6, 0.17], [10.5, 9.1, 153.0], [-22.5, 41.1, -3.9]]
            ),
            np.array([411289.44174598, 292979.90194606, 222079.88024992]),
        )
    ],
)
def test_get_moi_unit(point_masses, point_coords, expected_moi):
    np.testing.assert_allclose(
        expected_moi,
        get_moi(point_masses, point_coords, normalize=False),
        rtol=1e-12,
    )


def test_get_moi_integration():
    """
    Testing moment of inertia output of get_moi function with
    a constructed mesh.
    Theoretical normalized MoI of a homogeneous sphere is 0.4.
    The used mesh was defined with shape functions of the order 4 what makes
    the resulting MoI approximation fairly precise (down to 10^-8).XS
    """

    ms = Globe3D()
    ms.basic.elements_per_wavelength = 0.5
    ms.basic.min_period_in_seconds = 200.0
    ms.basic.model = "prem_iso_one_crust"
    ms.advanced.tensor_order = 4
    mesh = ms.create_mesh()

    mesh.elemental_fields["RHO"] = 1
    mass_matrix = get_mass_matrix(mesh)
    coord = mesh.points[mesh.connectivity]
    nelem, nodelem, ndim = coord.shape
    np.testing.assert_allclose(
        get_moi(mass_matrix.ravel(), coord.reshape(nelem * nodelem, ndim)),
        [0.4, 0.4, 0.4],
        rtol=1e-8,
    )
    print("--- test_get_MoI succeded --- ")



