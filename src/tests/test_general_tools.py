from general_tools import convert_to_iso_longitudes, 
import numpy as np
import pytest

def test_convert_to_iso_longitudes():
    test_inp = [[0., 15.23,  193, 358.5]]
    test_out = [[0., 15.23, -167,  -1.5]]
    np.testing.assert_equal( gt.convert_to_iso_longitudes(test_inp), test_out)
        
    with pytest.raises(AssertionError):
        tol = 1e-5
        gt.convert_to_iso_longitudes([0-tol, 360+tol])        
