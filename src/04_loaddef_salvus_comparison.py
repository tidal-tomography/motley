# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.15.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import netCDF4
import numpy as np
import xarray as xr
import pygmt

from toolbox.plotting_tools  import plot_DataArray
from toolbox.geodetic    import convert_to_iso_longitudes
# -

print(pygmt.__version__)
print(xr.__version__)

# +
LOADDEF_FILE = '../input/stations/cn_OceanOnly_GOT410c-M2_cm_convgf_GOT410c_global_PREM.txt'
SALVUS_FILE  = '../output/full_alpha_minimal_1x1_incore/solution/point_solution.nc'

PICTURE_FILE = ''

# +
#LoadDef results
ld_data = np.loadtxt(LOADDEF_FILE, skiprows=1)

lat     = np.unique(ld_data[:,1])
lon     = convert_to_iso_longitudes(np.unique(ld_data[:,2]))
shape   = (len(lat), len(lon)) 
displ_v = ld_data[:,7]

lon_order = np.argsort(lon)
ld_da   = xr.DataArray(data = displ_v.reshape(shape)[:,lon_order],
             dims = ['lat', 'lon'],
             coords=dict(lat = lat, 
                         lon = lon[lon_order]))
# -

#Salvus results
station_grid = xr.open_dataset(SALVUS_FILE)
sv_da = ((station_grid.displacement_re**2 + station_grid.displacement_im**2)**0.5*1000)[:-1,:,:].sel(VNE='V')

landmask30m = pygmt.datasets.load_earth_mask(resolution="30m")

landmask_mesh = landmask30m.sel(lat = sv_da.lat,
                                lon = sv_da.lon)

da[1]

# +
LANDMASK_ON = True
fig = pygmt.Figure()
with fig.subplot(nrows=1, ncols=3, subsize = ('10c', '6.5c'), autolabel=True, margins="2.c", ):
    da = [ld_da, sv_da, ld_da - sv_da]
    if LANDMASK_ON: da = [d*landmask_mesh for d in da]
    for p in range(2):
        with fig.set_panel(panel=p):
            labels = ['LoadDef', 'Salvus', 'Difference']
            plot_DataArray(fig, 
                           da = da[p],
                           cmap='magma',
                           #series = [0,45],
                           cb_name = labels[p],
                           cb_units = 'mm')
    p = 2
    with fig.set_panel(panel=p):
        plot_DataArray(fig, 
                       da = da[p],
                       cb_name = labels[p],
                       cmap='thermal',
                       series  = [],#[-10,10],
                       cb_units = 'mm')

#fig.savefig(PICTURE_FILE)
# -

fig.show(width = 1500)


